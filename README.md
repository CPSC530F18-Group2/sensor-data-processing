# Data Processing

For processing the data retrieved from the android app.  
Currently running the command `./runall.sh` produces the following output:  


    Parsing file: Data/2018_07_17_18_30_02_magneticfield.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.00%,   2.42%,  32.12%,   1.41%,   0.00%,  33.33%,   0.00%,   0.81%,  24.24%,   5.66%
    Statistical Distance: 0.4469696970
    Kullback Distance: 1.2641694385
        Minimum Entropy: 1.5849625007 bits
    Digit  1:  12.73%,   1.41%,   1.62%,   5.66%,   0.00%,  32.93%,  20.61%,  24.24%,   0.81%,   0.00%
    Statistical Distance: 0.4050505051
    Kullback Distance: 0.9768716687
        Minimum Entropy: 1.6025565607 bits
    Digit  2:  45.66%,   0.00%,  22.02%,   0.00%,   0.00%,  25.86%,   0.00%,   6.46%,   0.00%,   0.00%
    Statistical Distance: 0.3353535354
    Kullback Distance: 1.5647697171
        Minimum Entropy: 1.1311057526 bits
    Digit  3:  71.52%,   0.00%,   0.00%,   0.00%,   0.00%,  28.48%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.4599547040
        Minimum Entropy: 0.4836791649 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   7.68%,   0.40%,  39.60%,   1.41%,   0.00%,   4.85%,  27.88%,   0.00%,  18.18%,   0.00%
    Statistical Distance: 0.4065656566
    Kullback Distance: 1.2167809822
        Minimum Entropy: 1.3365748709 bits
    Digit  1:  12.53%,   0.00%,  27.88%,   0.00%,   0.00%,  39.60%,   0.00%,  19.60%,   0.40%,   0.00%
    Statistical Distance: 0.3459595960
    Kullback Distance: 1.4106660495
        Minimum Entropy: 1.3365748709 bits
    Digit  2:  52.12%,   0.00%,   0.00%,   0.00%,   0.00%,  47.47%,   0.00%,   0.40%,   0.00%,   0.00%
    Statistical Distance: 0.4459595960
    Kullback Distance: 2.2895890655
        Minimum Entropy: 0.9400574595 bits
    Digit  3:  99.60%,   0.00%,   0.00%,   0.00%,   0.00%,   0.40%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4959595960
    Kullback Distance: 3.2839844129
        Minimum Entropy: 0.0058408786 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  20.81%,   1.01%,  18.59%,   6.26%,   6.67%,   5.45%,  24.65%,   3.23%,  13.13%,   0.20%
    Statistical Distance: 0.3717171717
    Kullback Distance: 0.5320935049
        Minimum Entropy: 2.0205473774 bits
    Digit  1:  22.63%,   1.82%,  22.83%,   6.87%,   0.00%,  21.82%,   3.64%,  17.58%,   2.83%,   0.00%
    Statistical Distance: 0.3484848485
    Kullback Distance: 0.7404061859
        Minimum Entropy: 2.1311057526 bits
    Digit  2:  44.44%,   0.00%,   5.45%,   0.00%,   0.00%,  40.40%,   0.00%,   9.70%,   0.00%,   0.00%
    Statistical Distance: 0.3484848485
    Kullback Distance: 1.7183818396
        Minimum Entropy: 1.1699250014 bits
    Digit  3:  84.85%,   0.00%,   0.00%,   0.00%,   0.00%,  15.15%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.7083090753
        Minimum Entropy: 0.2370391973 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 0m 19s

    Parsing file: Data/2018_07_17_18_33_01_linearacceleration.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   3.27%,  25.38%,  27.89%,  15.58%,  18.57%,   9.04%,   0.28%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.3241307371
    Kullback Distance: 0.9387890049
        Minimum Entropy: 1.8423895340 bits
    Digit  2:  10.78%,   7.37%,   8.69%,  10.71%,  12.87%,  11.54%,   7.93%,   8.55%,  10.43%,  11.13%
    Statistical Distance: 0.0745479833
    Kullback Distance: 0.0205635657
        Minimum Entropy: 2.9584664999 bits
    Digit  3:   9.87%,  10.78%,   9.81%,   9.94%,  10.71%,   9.74%,  10.43%,   9.94%,  10.01%,   8.76%
    Statistical Distance: 0.0193324061
    Kullback Distance: 0.0021617343
        Minimum Entropy: 3.2137235552 bits
    Digit  4:   9.67%,  10.22%,  10.29%,   9.18%,  10.71%,   9.60%,  10.85%,   9.60%,   9.81%,  10.08%
    Statistical Distance: 0.0215577191
    Kullback Distance: 0.0018074974
        Minimum Entropy: 3.2044457416 bits
    Digit  5:  10.08%,  10.22%,   8.28%,  11.27%,  10.08%,  11.68%,   9.74%,   9.25%,   9.04%,  10.36%
    Statistical Distance: 0.0369958275
    Kullback Distance: 0.0066211106
        Minimum Entropy: 3.0975305377 bits
    Digit  6:   8.41%,  10.08%,  10.29%,  10.99%,   9.87%,  10.50%,   9.87%,   9.74%,   9.94%,  10.29%
    Statistical Distance: 0.0215577191
    Kullback Distance: 0.0029795401
        Minimum Entropy: 3.1860672123 bits
    Digit  7:   8.90%,   8.97%,  10.08%,   9.39%,   9.94%,  10.92%,  10.01%,   9.87%,  10.64%,  11.27%
    Statistical Distance: 0.0292072323
    Kullback Distance: 0.0039797380
        Minimum Entropy: 3.1499979576 bits
    Digit  8:  10.01%,   9.46%,  10.92%,  10.71%,   7.86%,   9.53%,   9.53%,  11.13%,  10.08%,  10.78%
    Statistical Distance: 0.0363004172
    Kullback Distance: 0.0063780072
        Minimum Entropy: 3.1679198656 bits
    Digit  9:   9.25%,   9.67%,  10.08%,  10.22%,   9.32%,  10.22%,  11.06%,   9.18%,  10.71%,  10.29%
    Statistical Distance: 0.0258692629
    Kullback Distance: 0.0026117415
        Minimum Entropy: 3.1769650052 bits
    Digit 10:  11.68%,  10.78%,   8.14%,  10.15%,  10.01%,   9.53%,   9.94%,  10.08%,   9.87%,   9.81%
    Statistical Distance: 0.0271210014
    Kullback Distance: 0.0052682527
        Minimum Entropy: 3.0975305377 bits
    Digit 11:  10.29%,   8.14%,  11.54%,  10.50%,  10.64%,   9.67%,  10.36%,   9.60%,   9.11%,  10.15%
    Statistical Distance: 0.0349095967
    Kullback Distance: 0.0057420704
        Minimum Entropy: 3.1148085291 bits
    Digit 12:   9.94%,  10.08%,   9.87%,   9.74%,   8.97%,   9.39%,  11.06%,  10.78%,   9.25%,  10.92%
    Statistical Distance: 0.0283727399
    Kullback Distance: 0.0033501263
        Minimum Entropy: 3.1769650052 bits
    Digit 13:  10.08%,   9.11%,  10.08%,  10.50%,   9.46%,  11.40%,  10.36%,   8.90%,  10.50%,   9.60%
    Statistical Distance: 0.0293463143
    Kullback Distance: 0.0036491472
        Minimum Entropy: 3.1322959558 bits
    Digit 14:  10.01%,   9.87%,   9.46%,  10.22%,   9.32%,   9.46%,   9.81%,  11.27%,  10.15%,  10.43%
    Statistical Distance: 0.0208623088
    Kullback Distance: 0.0021083685
        Minimum Entropy: 3.1499979576 bits
    Digit 15:  10.57%,   8.83%,   9.87%,   9.32%,  11.13%,  10.50%,  10.22%,   9.87%,  10.01%,   9.67%
    Statistical Distance: 0.0243393602
    Kullback Distance: 0.0027988588
        Minimum Entropy: 3.1679198656 bits
    Digit 16:   9.81%,  10.57%,  10.71%,   8.69%,  10.92%,  10.15%,   9.04%,   9.87%,   8.62%,  11.61%
    Statistical Distance: 0.0396383866
    Kullback Distance: 0.0064268624
        Minimum Entropy: 3.1061436680 bits
    Digit 17:  46.18%,   1.04%,   3.69%,   5.08%,   9.60%,  10.85%,  10.22%,   6.75%,   5.63%,   0.97%
    Statistical Distance: 0.3724617524
    Kullback Distance: 0.7750146539
        Minimum Entropy: 1.1148085291 bits
    Digit 18:  99.72%,   0.00%,   0.00%,   0.00%,   0.00%,   0.21%,   0.07%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5472183588
    Kullback Distance: 3.2920482264
        Minimum Entropy: 0.0040186517 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:  43.32%,  46.80%,   9.67%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4012517385
    Kullback Distance: 1.9420419801
        Minimum Entropy: 1.0953852658 bits
    Digit  2:  10.99%,   9.81%,  10.01%,  10.29%,  11.20%,  12.10%,   9.18%,   8.69%,   9.32%,   8.41%
    Statistical Distance: 0.0458970793
    Kullback Distance: 0.0087933788
        Minimum Entropy: 3.0469044646 bits
    Digit  3:  10.08%,  10.85%,   9.60%,   9.25%,  10.99%,   9.94%,  10.92%,   8.14%,  10.50%,   9.74%
    Statistical Distance: 0.0333796940
    Kullback Distance: 0.0052264003
        Minimum Entropy: 3.1860672123 bits
    Digit  4:   8.97%,   9.81%,  10.85%,  11.34%,   9.25%,   9.87%,   9.39%,  10.22%,   9.87%,  10.43%
    Statistical Distance: 0.0283727399
    Kullback Distance: 0.0034403442
        Minimum Entropy: 3.1411198062 bits
    Digit  5:  10.29%,  10.15%,   9.39%,  10.78%,  10.08%,   9.87%,   9.18%,   8.83%,  10.08%,  11.34%
    Statistical Distance: 0.0272600834
    Kullback Distance: 0.0035587626
        Minimum Entropy: 3.1411198062 bits
    Digit  6:  11.54%,   9.87%,   9.87%,  11.54%,  10.01%,   9.67%,  10.01%,   9.18%,   8.76%,   9.53%
    Statistical Distance: 0.0311543811
    Kullback Distance: 0.0051956923
        Minimum Entropy: 3.1148085291 bits
    Digit  7:   8.76%,   8.83%,  10.08%,  10.64%,  12.38%,   8.83%,   9.67%,  10.15%,  10.71%,   9.94%
    Statistical Distance: 0.0396383866
    Kullback Distance: 0.0077441210
        Minimum Entropy: 3.0141145295 bits
    Digit  8:   8.97%,  11.06%,   9.11%,  11.06%,   9.53%,  10.78%,  10.43%,   9.32%,   9.81%,   9.94%
    Statistical Distance: 0.0332406120
    Kullback Distance: 0.0040344349
        Minimum Entropy: 3.1769650052 bits
    Digit  9:   9.94%,   8.41%,   9.87%,  10.71%,  10.57%,  10.99%,   8.90%,   9.25%,  10.92%,  10.43%
    Statistical Distance: 0.0361613352
    Kullback Distance: 0.0052417731
        Minimum Entropy: 3.1860672123 bits
    Digit 10:  10.64%,   9.81%,  10.01%,   9.32%,  10.15%,   9.81%,  12.87%,   9.32%,   9.32%,   8.76%
    Statistical Distance: 0.0367176634
    Kullback Distance: 0.0079689399
        Minimum Entropy: 2.9584664999 bits
    Digit 11:  10.08%,   9.67%,   9.53%,  10.36%,   9.67%,   8.97%,   8.76%,  12.93%,   9.39%,  10.64%
    Statistical Distance: 0.0401947149
    Kullback Distance: 0.0086159563
        Minimum Entropy: 2.9506891493 bits
    Digit 12:   9.46%,   9.74%,  10.36%,  10.15%,   8.76%,  10.99%,  10.22%,  10.64%,  10.99%,   8.69%
    Statistical Distance: 0.0335187761
    Kullback Distance: 0.0045086886
        Minimum Entropy: 3.1860672123 bits
    Digit 13:   9.18%,  10.64%,  10.64%,  10.85%,   8.90%,   9.32%,  10.92%,   9.60%,  10.43%,   9.53%
    Statistical Distance: 0.0347705146
    Kullback Distance: 0.0038356510
        Minimum Entropy: 3.1952272115 bits
    Digit 14:  12.38%,   8.97%,   8.69%,   9.25%,   9.25%,   8.69%,  10.92%,   9.67%,  11.06%,  11.13%
    Statistical Distance: 0.0547983310
    Kullback Distance: 0.0103313288
        Minimum Entropy: 3.0141145295 bits
    Digit 15:  10.22%,   9.39%,   9.87%,   9.81%,   9.81%,  11.27%,  11.82%,   8.69%,   9.74%,   9.39%
    Statistical Distance: 0.0331015299
    Kullback Distance: 0.0053655557
        Minimum Entropy: 3.0804570243 bits
    Digit 16:  10.36%,  11.06%,   9.25%,   9.87%,  10.15%,  10.85%,  10.29%,   9.39%,   9.25%,   9.53%
    Statistical Distance: 0.0271210014
    Kullback Distance: 0.0027410865
        Minimum Entropy: 3.1769650052 bits
    Digit 17:  19.05%,   5.98%,   8.62%,   9.25%,  11.13%,   9.46%,  11.06%,  10.22%,   9.39%,   5.84%
    Statistical Distance: 0.1146036161
    Kullback Distance: 0.0789653231
        Minimum Entropy: 2.3918158775 bits
    Digit 18:  88.04%,   0.70%,   0.63%,   0.97%,   1.81%,   3.41%,   1.53%,   1.67%,   0.90%,   0.35%
    Statistical Distance: 0.7803894298
    Kullback Distance: 2.4480188279
        Minimum Entropy: 0.1837862710 bits
    Digit 19:  99.10%,   0.00%,   0.07%,   0.00%,   0.00%,   0.00%,   0.21%,   0.35%,   0.14%,   0.14%
    Statistical Distance: 0.6909596662
    Kullback Distance: 3.2282747579
        Minimum Entropy: 0.0131017565 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   0.00%,   0.07%,   0.28%,   7.79%,  48.12%,  38.73%,   4.87%,   0.14%,   0.00%,   0.00%
    Statistical Distance: 0.5185674548
    Kullback Distance: 1.7409330309
        Minimum Entropy: 1.0552197328 bits
    Digit  2:   9.60%,   8.34%,  12.31%,  12.66%,   7.30%,   7.51%,   8.69%,  15.16%,  10.50%,   7.93%
    Statistical Distance: 0.1062586926
    Kullback Distance: 0.0425443059
        Minimum Entropy: 2.7216636357 bits
    Digit  3:   9.81%,  10.22%,   8.69%,   9.67%,   9.04%,   8.97%,   9.39%,  11.75%,  10.43%,  12.03%
    Statistical Distance: 0.0443671766
    Kullback Distance: 0.0082086247
        Minimum Entropy: 3.0552197328 bits
    Digit  4:  11.89%,   9.60%,  10.29%,   9.87%,  10.71%,  10.15%,   8.34%,  10.43%,   8.83%,   9.87%
    Statistical Distance: 0.0347705146
    Kullback Distance: 0.0062585616
        Minimum Entropy: 3.0719954456 bits
    Digit  5:   9.87%,  10.15%,   8.97%,   9.87%,  10.36%,  11.82%,   9.11%,   9.67%,   9.25%,  10.92%
    Statistical Distance: 0.0325452017
    Kullback Distance: 0.0048636504
        Minimum Entropy: 3.0804570243 bits
    Digit  6:  10.08%,  10.01%,   9.60%,  11.06%,   9.53%,   8.97%,  10.92%,  11.61%,   9.11%,   9.11%
    Statistical Distance: 0.0368567455
    Kullback Distance: 0.0054114247
        Minimum Entropy: 3.1061436680 bits
    Digit  7:  10.36%,   8.07%,  10.08%,   9.67%,   9.25%,  10.78%,  10.01%,   9.81%,  10.85%,  11.13%
    Statistical Distance: 0.0321279555
    Kullback Distance: 0.0053280907
        Minimum Entropy: 3.1679198656 bits
    Digit  8:   9.46%,   9.67%,  11.61%,   9.74%,   7.93%,  10.43%,  10.99%,  11.68%,  11.34%,   7.16%
    Statistical Distance: 0.0605006954
    Kullback Distance: 0.0159029360
        Minimum Entropy: 3.0975305377 bits
    Digit  9:   9.60%,  10.08%,  10.29%,   8.97%,  10.64%,   9.87%,  11.47%,   9.67%,   9.46%,   9.94%
    Statistical Distance: 0.0248956885
    Kullback Distance: 0.0030728744
        Minimum Entropy: 3.1235257462 bits
    Digit 10:   8.62%,  11.20%,   9.32%,  10.78%,  10.36%,  10.15%,  10.29%,   9.74%,   9.46%,  10.08%
    Statistical Distance: 0.0286509040
    Kullback Distance: 0.0036407564
        Minimum Entropy: 3.1589310823 bits
    Digit 11:  11.06%,   9.25%,  10.29%,   8.90%,   7.86%,  11.34%,   9.74%,  10.57%,  10.64%,  10.36%
    Statistical Distance: 0.0425591099
    Kullback Distance: 0.0076323025
        Minimum Entropy: 3.1411198062 bits
    Digit 12:   9.39%,   9.11%,   9.53%,   9.67%,  10.57%,  10.50%,  10.99%,  10.64%,   9.94%,   9.67%
    Statistical Distance: 0.0269819193
    Kullback Distance: 0.0025724358
        Minimum Entropy: 3.1860672123 bits
    Digit 13:  10.71%,   8.69%,  10.50%,  10.29%,   9.53%,  10.08%,   9.18%,   9.67%,  10.29%,  11.06%
    Statistical Distance: 0.0293463143
    Kullback Distance: 0.0034736747
        Minimum Entropy: 3.1769650052 bits
    Digit 14:   9.87%,   9.11%,   9.46%,   9.81%,  10.01%,   9.87%,  11.82%,  10.15%,  10.78%,   9.11%
    Statistical Distance: 0.0276773296
    Kullback Distance: 0.0041496916
        Minimum Entropy: 3.0804570243 bits
    Digit 15:  11.47%,   8.76%,   9.87%,  10.85%,   9.94%,   9.53%,   9.67%,   9.39%,  10.08%,  10.43%
    Statistical Distance: 0.0283727399
    Kullback Distance: 0.0038268700
        Minimum Entropy: 3.1235257462 bits
    Digit 16:  10.57%,  10.50%,   9.39%,  10.92%,   8.28%,  10.50%,  10.22%,   9.74%,   9.46%,  10.43%
    Statistical Distance: 0.0314325452
    Kullback Distance: 0.0041672675
        Minimum Entropy: 3.1952272115 bits
    Digit 17:  70.03%,   0.00%,   0.00%,   0.28%,   9.94%,   9.11%,  10.22%,   0.35%,   0.07%,   0.00%
    Statistical Distance: 0.4525034771
    Kullback Distance: 1.9203140658
        Minimum Entropy: 0.5139999924 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Wrote 18970 bytes (151760 bits) to file: "Data/2018_07_17_18_33_01_linearacceleration.csv.bin"
    Generated 151760 bits of entropy from data collected over 0m 57s

    Parsing file: Data/2018_07_17_18_38_31_linearacceleration.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  83.00%,   2.49%,   2.68%,   1.89%,   1.89%,   1.89%,   1.49%,   1.89%,   1.29%,   1.49%
    Statistical Distance: 0.7300198807
    Kullback Distance: 2.1316450518
        Minimum Entropy: 0.2687822024 bits
    Digit  1:  34.49%,  34.29%,  17.00%,   4.08%,   1.79%,   1.99%,   1.49%,   1.19%,   1.69%,   1.99%
    Statistical Distance: 0.5578528827
    Kullback Distance: 1.0452519409
        Minimum Entropy: 1.5356227372 bits
    Digit  2:   9.15%,  10.34%,   9.74%,  10.83%,   8.35%,  10.83%,   9.15%,  12.33%,  10.83%,   8.45%
    Statistical Distance: 0.0516898608
    Kullback Distance: 0.0102294147
        Minimum Entropy: 3.0202182794 bits
    Digit  3:   9.24%,  10.44%,  11.63%,  10.44%,   8.15%,  10.64%,   9.94%,   9.15%,  11.43%,   8.95%
    Statistical Distance: 0.0457256461
    Kullback Distance: 0.0082229342
        Minimum Entropy: 3.1040498702 bits
    Digit  4:   8.45%,   9.54%,  10.04%,   9.24%,  10.44%,  10.24%,  10.34%,  10.64%,  11.13%,   9.94%
    Statistical Distance: 0.0282306163
    Kullback Distance: 0.0038486551
        Minimum Entropy: 3.1670596677 bits
    Digit  5:  10.44%,   8.65%,  11.13%,  10.24%,  11.43%,  10.44%,   9.84%,   9.54%,  10.24%,   8.05%
    Statistical Distance: 0.0391650099
    Kullback Distance: 0.0071481117
        Minimum Entropy: 3.1289245389 bits
    Digit  6:   9.94%,  10.44%,  10.14%,  11.73%,   9.24%,   9.54%,  10.44%,   9.74%,   8.55%,  10.24%
    Statistical Distance: 0.0298210736
    Kullback Distance: 0.0045955129
        Minimum Entropy: 3.0917715404 bits
    Digit  7:  10.93%,  11.13%,   9.15%,  10.34%,  10.24%,   9.54%,   9.05%,   8.75%,   8.95%,  11.93%
    Statistical Distance: 0.0457256461
    Kullback Distance: 0.0075390017
        Minimum Entropy: 3.0675239942 bits
    Digit  8:  10.83%,   9.34%,  10.44%,  10.24%,   9.64%,   9.44%,  11.03%,   9.94%,   9.84%,   9.24%
    Statistical Distance: 0.0254473161
    Kullback Distance: 0.0024940822
        Minimum Entropy: 3.1799987235 bits
    Digit  9:   9.05%,  11.63%,  11.03%,  10.74%,   9.15%,  10.14%,   9.05%,   8.45%,   9.84%,  10.93%
    Statistical Distance: 0.0447316103
    Kullback Distance: 0.0073231436
        Minimum Entropy: 3.1040498702 bits
    Digit 10:  10.34%,  10.34%,  10.04%,  10.24%,  10.34%,   9.54%,   9.74%,   9.74%,  10.64%,   9.05%
    Statistical Distance: 0.0192842942
    Kullback Distance: 0.0015014926
        Minimum Entropy: 3.2329476034 bits
    Digit 11:   8.85%,  10.54%,  10.74%,   9.84%,   9.84%,   9.84%,  11.93%,   7.95%,  10.14%,  10.34%
    Statistical Distance: 0.0367793241
    Kullback Distance: 0.0075143446
        Minimum Entropy: 3.0675239942 bits
    Digit 12:  11.63%,   9.44%,  11.83%,   9.64%,  11.03%,   7.55%,   9.84%,   9.84%,  10.14%,   9.05%
    Statistical Distance: 0.0463220676
    Kullback Distance: 0.0106106965
        Minimum Entropy: 3.0795968265 bits
    Digit 13:   9.44%,   9.15%,   9.94%,  11.63%,  10.04%,   8.95%,   9.24%,  10.93%,  10.24%,  10.44%
    Statistical Distance: 0.0328031809
    Kullback Distance: 0.0046358549
        Minimum Entropy: 3.1040498702 bits
    Digit 14:  10.54%,   9.34%,   9.05%,  10.74%,  10.74%,   9.54%,   8.95%,   9.64%,  10.83%,  10.64%
    Statistical Distance: 0.0347912525
    Kullback Distance: 0.0038154100
        Minimum Entropy: 3.2062302650 bits
    Digit 15:  14.81%,   8.75%,   8.55%,  10.14%,   9.64%,   9.34%,  10.14%,   8.45%,  10.14%,  10.04%
    Statistical Distance: 0.0526838966
    Kullback Distance: 0.0195885574
        Minimum Entropy: 2.7552460693 bits
    Digit 16:  24.85%,   8.35%,   7.65%,   8.35%,   7.95%,   9.84%,   8.45%,   8.05%,   8.95%,   7.55%
    Statistical Distance: 0.1485089463
    Kullback Distance: 0.1341873265
        Minimum Entropy: 2.0086303051 bits
    Digit 17:  35.09%,   4.47%,   9.44%,   7.95%,   7.16%,   7.75%,   8.95%,   7.65%,   6.96%,   4.57%
    Statistical Distance: 0.2508946322
    Kullback Distance: 0.3545582368
        Minimum Entropy: 1.5108902165 bits
    Digit 18:  91.55%,   0.30%,   0.40%,   1.49%,   1.39%,   2.49%,   1.09%,   1.09%,   0.10%,   0.10%
    Statistical Distance: 0.8155069583
    Kullback Distance: 2.6775382878
        Minimum Entropy: 0.1273572437 bits
    Digit 19:  99.20%,   0.10%,   0.00%,   0.00%,   0.20%,   0.10%,   0.20%,   0.10%,   0.00%,   0.10%
    Statistical Distance: 0.7420477137
    Kullback Distance: 3.2351578523
        Minimum Entropy: 0.0115185845 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  82.80%,   3.18%,   1.59%,   1.49%,   1.99%,   2.49%,   1.69%,   1.19%,   1.49%,   2.09%
    Statistical Distance: 0.7280318091
    Kullback Distance: 2.1252437393
        Minimum Entropy: 0.2722419044 bits
    Digit  1:  65.51%,  19.18%,   2.39%,   2.58%,   0.99%,   2.29%,   2.19%,   1.59%,   1.29%,   1.99%
    Statistical Distance: 0.6469184891
    Kullback Distance: 1.6004682350
        Minimum Entropy: 0.6102799348 bits
    Digit  2:  14.51%,  13.12%,  10.54%,  11.43%,   9.24%,  10.44%,   6.96%,   8.05%,   9.64%,   6.06%
    Statistical Distance: 0.1003976143
    Kullback Distance: 0.0449757982
        Minimum Entropy: 2.7845900309 bits
    Digit  3:   9.05%,   9.05%,  11.53%,  12.82%,  10.74%,   9.34%,   8.15%,   9.15%,  10.24%,   9.94%
    Statistical Distance: 0.0532803181
    Kullback Distance: 0.0121597037
        Minimum Entropy: 2.9631873344 bits
    Digit  4:   9.74%,   9.84%,  10.83%,  10.24%,   9.24%,   9.44%,  10.64%,   9.74%,   8.05%,  12.23%
    Statistical Distance: 0.0393638171
    Kullback Distance: 0.0078546927
        Minimum Entropy: 3.0319000845 bits
    Digit  5:   9.24%,  11.03%,   9.74%,  10.64%,  10.83%,  10.34%,  10.14%,   9.84%,  10.24%,   7.95%
    Statistical Distance: 0.0322067594
    Kullback Distance: 0.0054019515
        Minimum Entropy: 3.1799987235 bits
    Digit  6:   8.95%,   9.74%,   8.85%,  11.93%,   9.44%,   8.95%,  10.14%,  10.83%,  10.14%,  11.03%
    Statistical Distance: 0.0407554672
    Kullback Distance: 0.0067238172
        Minimum Entropy: 3.0675239942 bits
    Digit  7:  10.83%,   8.65%,   9.15%,  10.14%,   9.24%,   9.05%,  10.44%,  12.13%,  10.44%,   9.94%
    Statistical Distance: 0.0397614314
    Kullback Distance: 0.0068589076
        Minimum Entropy: 3.0436772522 bits
    Digit  8:  10.54%,   9.24%,   9.54%,  10.04%,   9.74%,  11.03%,  10.54%,  10.04%,   9.15%,  10.14%
    Statistical Distance: 0.0232604374
    Kullback Distance: 0.0023373978
        Minimum Entropy: 3.1799987235 bits
    Digit  9:   8.55%,  10.14%,  10.44%,  10.14%,   8.45%,   9.94%,  11.73%,  10.74%,  10.44%,   9.44%
    Statistical Distance: 0.0361829026
    Kullback Distance: 0.0063852335
        Minimum Entropy: 3.0917715404 bits
    Digit 10:   8.95%,   8.75%,   9.74%,  10.14%,   9.54%,  10.24%,  11.03%,   9.64%,  10.93%,  11.03%
    Statistical Distance: 0.0337972167
    Kullback Distance: 0.0044648983
        Minimum Entropy: 3.1799987235 bits
    Digit 11:  11.13%,   9.64%,   8.15%,  10.44%,  10.74%,  10.14%,  10.64%,   9.15%,   9.94%,  10.04%
    Statistical Distance: 0.0312127237
    Kullback Distance: 0.0049839047
        Minimum Entropy: 3.1670596677 bits
    Digit 12:   8.25%,  10.04%,  10.83%,  10.54%,   9.74%,   9.74%,   9.64%,  11.93%,  10.74%,   8.55%
    Statistical Distance: 0.0407554672
    Kullback Distance: 0.0077396064
        Minimum Entropy: 3.0675239942 bits
    Digit 13:   8.55%,   9.34%,  10.54%,  10.93%,   8.55%,   9.34%,  11.53%,  10.14%,  11.63%,   9.44%
    Statistical Distance: 0.0477137177
    Kullback Distance: 0.0083201789
        Minimum Entropy: 3.1040498702 bits
    Digit 14:  10.24%,  10.14%,  10.24%,   9.44%,  11.33%,   9.24%,   9.05%,  10.14%,  10.04%,  10.14%
    Statistical Distance: 0.0226640159
    Kullback Distance: 0.0026799555
        Minimum Entropy: 3.1415245756 bits
    Digit 15:  14.81%,   8.15%,   9.34%,   9.05%,   9.74%,   8.45%,  11.43%,   9.44%,  10.83%,   8.75%
    Statistical Distance: 0.0707753479
    Kullback Distance: 0.0233440667
        Minimum Entropy: 2.7552460693 bits
    Digit 16:  24.95%,   8.05%,   7.95%,   8.35%,   9.54%,   8.05%,   8.35%,   9.34%,   8.35%,   7.06%
    Statistical Distance: 0.1495029821
    Kullback Distance: 0.1362323602
        Minimum Entropy: 2.0028710359 bits
    Digit 17:  29.13%,   7.65%,   9.54%,   6.86%,   8.55%,   8.65%,   6.66%,   8.45%,   8.15%,   6.36%
    Statistical Distance: 0.1912524851
    Kullback Distance: 0.2133099587
        Minimum Entropy: 1.7796577354 bits
    Digit 18:  75.84%,   0.70%,   2.78%,   3.48%,   4.47%,   4.47%,   3.28%,   2.39%,   1.59%,   0.99%
    Statistical Distance: 0.6584493042
    Kullback Distance: 1.8046794482
        Minimum Entropy: 0.3988753430 bits
    Digit 19:  98.01%,   0.20%,   0.00%,   0.20%,   0.20%,   0.40%,   0.30%,   0.60%,   0.00%,   0.10%
    Statistical Distance: 0.7801192843
    Kullback Distance: 3.1292951007
        Minimum Entropy: 0.0289707534 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  83.60%,   1.49%,   2.39%,   2.09%,   1.89%,   1.49%,   2.09%,   2.09%,   1.89%,   0.99%
    Statistical Distance: 0.7359840954
    Kullback Distance: 2.1643389283
        Minimum Entropy: 0.2584525996 bits
    Digit  1:   2.29%,   1.59%,   2.29%,   5.47%,  12.52%,  36.48%,  28.93%,   5.47%,   2.09%,   2.88%
    Statistical Distance: 0.4793240557
    Kullback Distance: 0.8314061822
        Minimum Entropy: 1.4547783370 bits
    Digit  2:   8.65%,  10.04%,  12.62%,   8.35%,   8.55%,   7.65%,  10.64%,  12.03%,  11.43%,  10.04%
    Statistical Distance: 0.0679920477
    Kullback Distance: 0.0184524017
        Minimum Entropy: 2.9857299030 bits
    Digit  3:  11.23%,   9.34%,   9.54%,   8.55%,  11.13%,   9.44%,  12.03%,   8.25%,  11.83%,   8.65%
    Statistical Distance: 0.0622266402
    Kullback Distance: 0.0130372629
        Minimum Entropy: 3.0555513525 bits
    Digit  4:   9.44%,  12.13%,   8.15%,  12.13%,   9.74%,  11.33%,  10.14%,   7.65%,   8.75%,  10.54%
    Statistical Distance: 0.0626242545
    Kullback Distance: 0.0159687566
        Minimum Entropy: 3.0436772522 bits
    Digit  5:  10.14%,  10.64%,  11.03%,  10.34%,   9.34%,   9.74%,  10.14%,   9.94%,   7.85%,  10.83%
    Statistical Distance: 0.0312127237
    Kullback Distance: 0.0055919697
        Minimum Entropy: 3.1799987235 bits
    Digit  6:   8.45%,   8.85%,  11.03%,  10.74%,   9.74%,  10.64%,  10.24%,  10.44%,  10.74%,   9.15%
    Statistical Distance: 0.0381709742
    Kullback Distance: 0.0053921656
        Minimum Entropy: 3.1799987235 bits
    Digit  7:  11.33%,  10.14%,  10.64%,   9.74%,   9.24%,   7.65%,  10.04%,  11.43%,  11.03%,   8.75%
    Statistical Distance: 0.0461232604
    Kullback Distance: 0.0096611900
        Minimum Entropy: 3.1289245389 bits
    Digit  8:   9.34%,  12.43%,  10.64%,   8.85%,  10.54%,  11.93%,   9.24%,   8.05%,   9.05%,   9.94%
    Statistical Distance: 0.0552683897
    Kullback Distance: 0.0123073630
        Minimum Entropy: 3.0086303051 bits
    Digit  9:  10.83%,   9.24%,   8.75%,   9.15%,  10.14%,   9.44%,  10.64%,  12.03%,  11.73%,   8.05%
    Statistical Distance: 0.0536779324
    Kullback Distance: 0.0109273820
        Minimum Entropy: 3.0555513525 bits
    Digit 10:  11.33%,   8.75%,  11.33%,   9.64%,   8.65%,  11.83%,   9.34%,   9.84%,   9.74%,   9.54%
    Statistical Distance: 0.0449304175
    Kullback Distance: 0.0079264987
        Minimum Entropy: 3.0795968265 bits
    Digit 11:  10.24%,   9.74%,  11.13%,   9.74%,  10.54%,  10.44%,  10.34%,   8.45%,   9.15%,  10.24%
    Statistical Distance: 0.0292246521
    Kullback Distance: 0.0038683729
        Minimum Entropy: 3.1670596677 bits
    Digit 12:   9.94%,  10.24%,   8.35%,  10.54%,   9.34%,   9.64%,  11.83%,   9.54%,   8.95%,  11.63%
    Statistical Distance: 0.0423459245
    Kullback Distance: 0.0078231149
        Minimum Entropy: 3.0795968265 bits
    Digit 13:  11.23%,  11.13%,  10.64%,  10.34%,  11.33%,  10.14%,   8.75%,   8.45%,   8.55%,   9.44%
    Statistical Distance: 0.0481113320
    Kullback Distance: 0.0083949986
        Minimum Entropy: 3.1415245756 bits
    Digit 14:  11.93%,   7.16%,  10.34%,  10.44%,  10.34%,  10.24%,  11.23%,   9.34%,   9.24%,   9.74%
    Statistical Distance: 0.0451292247
    Kullback Distance: 0.0111846538
        Minimum Entropy: 3.0675239942 bits
    Digit 15:  15.61%,   9.54%,  11.03%,   9.15%,   9.15%,   9.15%,   9.05%,   9.44%,   9.05%,   8.85%
    Statistical Distance: 0.0664015905
    Kullback Distance: 0.0244422341
        Minimum Entropy: 2.6797938409 bits
    Digit 16:  25.25%,   7.75%,   8.65%,   8.15%,   9.84%,   7.95%,   9.64%,   8.25%,   6.56%,   7.95%
    Statistical Distance: 0.1524850895
    Kullback Distance: 0.1440415995
        Minimum Entropy: 1.9857299030 bits
    Digit 17:  80.91%,   0.10%,   0.20%,   0.30%,   6.46%,   5.67%,   6.16%,   0.20%,   0.00%,   0.00%
    Statistical Distance: 0.6091451292
    Kullback Distance: 2.2663167601
        Minimum Entropy: 0.3055296055 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Wrote 12396 bytes (99168 bits) to file: "Data/2018_07_17_18_38_31_linearacceleration.csv.bin"
    Generated 99168 bits of entropy from data collected over 6m 53s

    Parsing file: Data/2018_07_17_18_46_37_accelerometer.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  99.19%,   0.06%,   0.09%,   0.06%,   0.09%,   0.04%,   0.12%,   0.13%,   0.15%,   0.07%
    Statistical Distance: 0.8919005983
    Kullback Distance: 3.2292189947
        Minimum Entropy: 0.0117325444 bits
    Digit  1:  75.12%,  22.58%,   1.71%,   0.11%,   0.08%,   0.07%,   0.07%,   0.08%,   0.10%,   0.06%
    Statistical Distance: 0.7769903359
    Kullback Distance: 2.3666636367
        Minimum Entropy: 0.4126935588 bits
    Digit  2:  17.06%,   0.10%,   0.07%,   1.46%,  48.67%,   0.06%,   0.09%,   0.09%,   0.27%,  32.11%
    Statistical Distance: 0.6784629544
    Kullback Distance: 1.6996068544
        Minimum Entropy: 1.0388940926 bits
    Digit  3:  17.02%,   6.42%,   0.06%,  16.29%,   0.10%,  25.79%,   0.06%,  32.69%,   0.08%,   1.50%
    Statistical Distance: 0.5179015186
    Kullback Distance: 1.0535397255
        Minimum Entropy: 1.6129846877 bits
    Digit  4:  17.06%,   0.09%,   0.07%,   0.08%,   0.33%,   1.45%,   6.38%,  16.26%,  25.84%,  32.43%
    Statistical Distance: 0.5158766682
    Kullback Distance: 1.0344373519
        Minimum Entropy: 1.6248117946 bits
    Digit  5:  99.27%,   0.12%,   0.14%,   0.09%,   0.06%,   0.13%,   0.06%,   0.04%,   0.04%,   0.06%
    Statistical Distance: 0.8927289462
    Kullback Distance: 3.2379115433
        Minimum Entropy: 0.0105282356 bits
    Digit  6:  75.14%,  23.95%,   0.30%,   0.08%,   0.07%,   0.09%,   0.10%,   0.09%,   0.08%,   0.08%
    Statistical Distance: 0.7908881730
    Kullback Distance: 2.4311371492
        Minimum Entropy: 0.4123400870 bits
    Digit  7:  17.09%,  16.26%,   0.09%,  32.72%,   0.07%,   6.35%,   0.07%,  25.81%,   0.07%,   1.45%
    Statistical Distance: 0.5188219052
    Kullback Distance: 1.0549024513
        Minimum Entropy: 1.6117667096 bits
    Digit  8:  17.10%,   0.06%,   0.06%,   0.10%,   0.31%,   0.08%,   1.51%,  22.55%,  25.80%,  32.42%
    Statistical Distance: 0.5786470318
    Kullback Distance: 1.2210080740
        Minimum Entropy: 1.6252213602 bits
    Digit  9:  23.36%,   0.12%,  32.46%,  16.24%,   0.06%,  25.73%,   0.29%,   0.10%,   1.51%,   0.11%
    Statistical Distance: 0.5780027612
    Kullback Distance: 1.2196382488
        Minimum Entropy: 1.6231746936 bits
    Digit 10:  18.47%,   0.15%,   0.13%,  41.95%,   0.12%,   0.04%,  38.73%,   0.27%,   0.06%,   0.08%
    Statistical Distance: 0.6915324436
    Kullback Distance: 1.7359231976
        Minimum Entropy: 1.2532153448 bits
    Digit 11:  17.06%,   0.11%,   1.47%,   0.28%,  25.78%,   0.06%,  16.31%,  32.46%,   0.10%,   6.37%
    Statistical Distance: 0.5161527842
    Kullback Distance: 1.0358404239
        Minimum Entropy: 1.6231746936 bits
    Digit 12:  17.22%,   0.08%,   0.08%,  32.44%,   0.06%,  22.56%,   1.52%,  25.79%,   0.11%,   0.13%
    Statistical Distance: 0.5801196503
    Kullback Distance: 1.2304378749
        Minimum Entropy: 1.6239930119 bits
    Digit 13:  17.11%,  17.66%,   0.29%,   0.10%,   0.13%,   0.04%,   6.37%,   0.10%,  25.78%,  32.43%
    Statistical Distance: 0.5297745053
    Kullback Distance: 1.0994606052
        Minimum Entropy: 1.6248117946 bits
    Digit 14:  49.42%,  41.97%,   0.09%,   6.59%,   0.07%,   0.10%,   1.52%,   0.07%,   0.07%,   0.09%
    Statistical Distance: 0.7138518178
    Kullback Distance: 1.8917551620
        Minimum Entropy: 1.0169628645 bits
    Digit 15:  17.57%,   1.44%,   0.23%,   0.06%,   0.04%,   0.05%,  16.24%,   0.03%,   6.31%,  58.05%
    Statistical Distance: 0.6185457892
    Kullback Distance: 1.6215837525
        Minimum Entropy: 0.7846623362 bits
    Digit 16:  17.81%,  16.19%,   1.60%,  25.72%,   0.02%,   0.00%,  38.66%,   0.01%,   0.00%,   0.00%
    Statistical Distance: 0.4337091578
    Kullback Distance: 1.3203780508
        Minimum Entropy: 1.3712269415 bits
    Digit 17:  19.27%,   0.00%,  16.19%,  25.70%,   0.20%,   0.00%,  38.64%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.3479751496
    Kullback Distance: 1.3869075858
        Minimum Entropy: 1.3719141028 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.08%,  99.20%,   0.09%,   0.14%,   0.15%,   0.08%,   0.08%,   0.07%,   0.01%,   0.09%
    Statistical Distance: 0.8919926369
    Kullback Distance: 3.2305498570
        Minimum Entropy: 0.0115986827 bits
    Digit  1:   0.09%,   0.08%,   0.09%,   0.10%,   0.79%,  14.71%,  50.28%,  29.87%,   3.79%,   0.19%
    Statistical Distance: 0.6485503912
    Kullback Distance: 1.6069527203
        Minimum Entropy: 0.9919228464 bits
    Digit  2:   0.09%,   0.11%,  45.34%,   3.75%,   0.15%,   0.04%,   0.63%,  38.06%,  11.73%,   0.12%
    Statistical Distance: 0.6512195122
    Kullback Distance: 1.6383992914
        Minimum Entropy: 1.1411996416 bits
    Digit  3:  14.28%,   0.06%,  13.61%,   0.11%,  20.51%,   0.11%,  28.17%,   0.17%,  22.91%,   0.07%
    Statistical Distance: 0.4947537966
    Kullback Distance: 1.0073522240
        Minimum Entropy: 1.8276132245 bits
    Digit  4:   0.28%,   0.63%,   3.26%,  10.08%,  19.92%,  28.13%,  22.26%,  11.04%,   3.67%,   0.74%
    Statistical Distance: 0.4143120110
    Kullback Distance: 0.7188573578
        Minimum Entropy: 1.8299717260 bits
    Digit  5:   0.11%,  99.25%,   0.19%,   0.11%,   0.05%,   0.05%,   0.04%,   0.08%,   0.03%,   0.09%
    Statistical Distance: 0.8925448688
    Kullback Distance: 3.2366615952
        Minimum Entropy: 0.0107957729 bits
    Digit  6:   0.18%,   0.14%,  15.30%,  50.28%,  33.12%,   0.71%,   0.12%,   0.06%,   0.05%,   0.06%
    Statistical Distance: 0.6869305108
    Kullback Distance: 1.7717223316
        Minimum Entropy: 0.9919228464 bits
    Digit  7:  19.90%,   0.70%,   0.16%,  22.82%,  10.07%,   3.63%,   0.17%,  28.15%,   0.18%,  14.24%
    Statistical Distance: 0.4516797055
    Kullback Distance: 0.8531228825
        Minimum Entropy: 1.8290278627 bits
    Digit  8:  28.12%,   0.11%,  22.31%,   0.07%,  11.12%,  10.05%,   4.21%,   0.10%,  23.78%,   0.13%
    Statistical Distance: 0.4537965946
    Kullback Distance: 0.9129113443
        Minimum Entropy: 1.8304438894 bits
    Digit  9:  19.95%,  28.07%,  22.30%,   0.06%,  11.16%,   4.23%,   3.93%,   0.11%,   0.16%,  10.03%
    Statistical Distance: 0.4151403590
    Kullback Distance: 0.7668454388
        Minimum Entropy: 1.8328070267 bits
    Digit 10:  10.03%,   0.14%,  11.16%,  19.92%,   0.10%,   4.21%,  28.10%,   0.05%,   3.99%,  22.30%
    Statistical Distance: 0.4151403590
    Kullback Distance: 0.7688145498
        Minimum Entropy: 1.8313886799 bits
    Digit 11:   3.91%,   0.09%,   0.08%,   0.09%,   0.08%,   0.19%,  15.34%,  50.31%,  29.83%,   0.06%
    Statistical Distance: 0.6548090198
    Kullback Distance: 1.6451082220
        Minimum Entropy: 0.9911308095 bits
    Digit 12:   0.09%,  23.80%,   0.10%,  22.26%,   0.16%,  14.13%,   0.17%,  28.11%,   0.06%,  11.13%
    Statistical Distance: 0.4942936033
    Kullback Distance: 1.0252775778
        Minimum Entropy: 1.8309162073 bits
    Digit 13:   0.80%,  17.81%,  22.92%,  47.92%,  10.03%,   0.07%,   0.05%,   0.13%,   0.17%,   0.10%
    Statistical Distance: 0.5868384722
    Kullback Distance: 1.4438604345
        Minimum Entropy: 1.0611638627 bits
    Digit 14:   3.61%,   0.69%,  28.14%,   0.72%,   3.27%,  22.25%,  10.12%,   0.13%,  11.11%,  19.96%
    Statistical Distance: 0.4158766682
    Kullback Distance: 0.7267394245
        Minimum Entropy: 1.8294997172 bits
    Digit 15:  11.26%,  22.29%,  28.06%,  19.93%,  10.10%,   0.06%,   0.08%,   3.34%,   1.21%,   3.68%
    Statistical Distance: 0.4163368615
    Kullback Distance: 0.7417967986
        Minimum Entropy: 1.8332801191 bits
    Digit 16:   0.83%,   0.00%,   0.71%,  36.78%,  47.87%,  13.72%,   0.09%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4837091578
    Kullback Distance: 1.7720405472
        Minimum Entropy: 1.0628272315 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.05%,   0.04%,   0.07%,   0.02%,   0.09%,   0.06%,   0.12%,   0.10%,  99.27%,   0.18%
    Statistical Distance: 0.8927289462
    Kullback Distance: 3.2385589766
        Minimum Entropy: 0.0105282356 bits
    Digit  1:   0.09%,   0.09%,   0.37%,   5.79%,  32.22%,  44.77%,  15.02%,   1.45%,   0.15%,   0.06%
    Statistical Distance: 0.6201104464
    Kullback Distance: 1.4711186186
        Minimum Entropy: 1.1594730406 bits
    Digit  2:   0.14%,   0.07%,  11.82%,  37.55%,   0.08%,   0.13%,   0.10%,  24.90%,  24.85%,   0.36%
    Statistical Distance: 0.5911642890
    Kullback Distance: 1.3477670600
        Minimum Entropy: 1.4130471172 bits
    Digit  3:   0.39%,  24.74%,   1.16%,  20.43%,   4.28%,  11.90%,  10.74%,   4.55%,   0.07%,  21.74%
    Statistical Distance: 0.3955361252
    Kullback Distance: 0.6549431940
        Minimum Entropy: 2.0150831313 bits
    Digit  4:  20.47%,  24.43%,  20.39%,  11.89%,   4.53%,   1.42%,   0.60%,   1.21%,   4.31%,  10.77%
    Statistical Distance: 0.3794293603
    Kullback Distance: 0.5718459050
        Minimum Entropy: 2.0334478987 bits
    Digit  5:  95.02%,   4.42%,   0.05%,   0.04%,   0.06%,   0.04%,   0.08%,   0.06%,   0.12%,   0.12%
    Statistical Distance: 0.8502070870
    Kullback Distance: 2.9950693612
        Minimum Entropy: 0.0736861280 bits
    Digit  6:   0.03%,   0.18%,   4.31%,  28.81%,   0.10%,   1.15%,  32.53%,   0.08%,   0.40%,  32.42%
    Statistical Distance: 0.6375057524
    Kullback Distance: 1.4111636891
        Minimum Entropy: 1.6203142295 bits
    Digit  7:   0.05%,  44.78%,  10.75%,   4.31%,   4.48%,  11.94%,  20.42%,   0.15%,   1.17%,   1.96%
    Statistical Distance: 0.4788771284
    Kullback Distance: 1.0214859547
        Minimum Entropy: 1.1591764644 bits
    Digit  8:  20.95%,   4.27%,   0.11%,  24.41%,  13.03%,  10.75%,   1.42%,   0.08%,   4.52%,  20.46%
    Statistical Distance: 0.3959963185
    Kullback Distance: 0.6529828741
        Minimum Entropy: 2.0345354940 bits
    Digit  9:   0.10%,   1.39%,   0.14%,   1.67%,  24.41%,  31.06%,   4.35%,   0.10%,   4.53%,  32.25%
    Statistical Distance: 0.5772204326
    Kullback Distance: 1.1584109162
        Minimum Entropy: 1.6326134945 bits
    Digit 10:  33.60%,  28.89%,   0.08%,   0.06%,   0.04%,   0.11%,   0.09%,   0.08%,   4.37%,  32.67%
    Statistical Distance: 0.6516797055
    Kullback Distance: 1.5037417078
        Minimum Entropy: 1.5733246006 bits
    Digit 11:   0.06%,   0.09%,   0.05%,   0.06%,   0.06%,  28.84%,  21.54%,  12.46%,  32.45%,   4.38%
    Statistical Distance: 0.5529682467
    Kullback Distance: 1.1944697830
        Minimum Entropy: 1.6235837947 bits
    Digit 12:   0.13%,  10.74%,  11.88%,   0.11%,   5.42%,  44.76%,   0.35%,   0.36%,  20.41%,   5.84%
    Statistical Distance: 0.4779567418
    Kullback Distance: 1.0759116264
        Minimum Entropy: 1.1597696778 bits
    Digit 13:   0.17%,   4.76%,  37.54%,  40.75%,  11.81%,   4.53%,   0.10%,   0.14%,   0.08%,   0.12%
    Statistical Distance: 0.6009664059
    Kullback Distance: 1.4290409053
        Minimum Entropy: 1.2952868960 bits
    Digit 14:   0.17%,   0.10%,   0.12%,   4.34%,   0.29%,  32.15%,  21.76%,  36.50%,   4.47%,   0.09%
    Statistical Distance: 0.6040957202
    Kullback Distance: 1.3175793771
        Minimum Entropy: 1.4539315918 bits
    Digit 15:  99.86%,   0.01%,   0.01%,   0.02%,   0.01%,   0.03%,   0.00%,   0.03%,   0.04%,   0.00%
    Statistical Distance: 0.7986194202
    Kullback Distance: 3.3032233812
        Minimum Entropy: 0.0019931318 bits
    Digit 16:  99.99%,   0.00%,   0.01%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4999079613
    Kullback Distance: 3.3205613182
        Minimum Entropy: 0.0001327898 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 7m 16s

    Parsing file: Data/2018_07_17_18_54_40_gravity.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   6.78%,  20.24%,  58.59%,  14.38%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.3321569575
    Kullback Distance: 1.7378754433
        Minimum Entropy: 0.7711952588 bits
    Digit  2:   9.73%,   7.55%,   8.70%,  11.01%,   8.61%,   9.36%,   9.68%,  11.39%,  12.98%,  11.00%
    Statistical Distance: 0.0637015551
    Kullback Distance: 0.0164728099
        Minimum Entropy: 2.9455909642 bits
    Digit  3:  10.17%,  10.04%,   9.73%,   9.30%,  10.20%,  10.45%,  10.15%,   9.68%,  10.47%,   9.81%
    Statistical Distance: 0.0147772138
    Kullback Distance: 0.0008811508
        Minimum Entropy: 3.2561460275 bits
    Digit  4:  10.62%,   9.92%,  10.14%,   9.78%,  10.00%,   9.98%,   9.91%,  10.37%,   9.72%,   9.56%
    Statistical Distance: 0.0113367174
    Kullback Distance: 0.0006268153
        Minimum Entropy: 3.2351970879 bits
    Digit  5:  10.43%,   9.78%,  10.30%,  10.03%,  10.30%,   9.81%,   9.68%,   9.95%,   9.85%,   9.86%
    Statistical Distance: 0.0106034969
    Kullback Distance: 0.0004264245
        Minimum Entropy: 3.2605953604 bits
    Digit  6:  10.00%,  10.02%,  10.08%,   9.99%,   9.83%,   9.62%,  10.33%,  10.25%,   9.85%,  10.02%
    Statistical Distance: 0.0070501974
    Kullback Distance: 0.0002688777
        Minimum Entropy: 3.2751511963 bits
    Digit  7:   9.84%,  10.43%,  10.00%,  10.50%,  10.04%,   9.96%,   9.81%,   9.85%,   9.77%,   9.81%
    Statistical Distance: 0.0097252437
    Kullback Distance: 0.0004421605
        Minimum Entropy: 3.2517103744 bits
    Digit  8:   9.89%,  10.19%,  10.20%,   9.93%,  10.00%,   9.84%,   9.85%,  10.40%,   9.70%,  10.00%
    Statistical Distance: 0.0079526227
    Kullback Distance: 0.0002832854
        Minimum Entropy: 3.2650584576 bits
    Digit  9:  10.13%,  10.15%,   9.73%,  10.14%,  10.18%,  10.02%,  10.12%,  10.15%,   9.95%,   9.43%
    Statistical Distance: 0.0089678511
    Kullback Distance: 0.0003916507
        Minimum Entropy: 3.2955509948 bits
    Digit 10:  10.18%,  10.14%,  10.16%,  10.13%,   9.96%,   9.84%,   9.73%,  10.03%,   9.97%,   9.87%
    Statistical Distance: 0.0064056079
    Kullback Distance: 0.0001563943
        Minimum Entropy: 3.2966928192 bits
    Digit 11:  10.07%,  10.18%,  10.06%,  10.17%,   9.14%,  10.18%,  10.14%,  10.10%,   9.85%,  10.09%
    Statistical Distance: 0.0100878253
    Kullback Distance: 0.0006727644
        Minimum Entropy: 3.2955509948 bits
    Digit 12:   9.91%,  10.21%,  10.03%,   9.73%,  10.24%,   9.74%,   9.56%,   9.88%,  10.47%,  10.24%
    Statistical Distance: 0.0118846185
    Kullback Distance: 0.0005340662
        Minimum Entropy: 3.2561460275 bits
    Digit 13:   9.92%,   9.77%,   9.97%,  10.13%,  10.35%,   9.74%,  10.35%,   9.68%,  10.16%,   9.93%
    Statistical Distance: 0.0099589074
    Kullback Distance: 0.0003801813
        Minimum Entropy: 3.2717790989 bits
    Digit 14:   9.87%,  10.51%,  10.18%,   9.98%,   9.64%,  10.19%,  10.03%,  10.41%,   9.92%,   9.26%
    Statistical Distance: 0.0132543711
    Kullback Distance: 0.0008704423
        Minimum Entropy: 3.2506035887 bits
    Digit 15:  10.16%,   9.90%,  10.18%,  10.11%,  10.09%,  10.10%,   9.75%,   9.37%,  10.35%,   9.98%
    Statistical Distance: 0.0099427927
    Kullback Distance: 0.0004978366
        Minimum Entropy: 3.2717790989 bits
    Digit 16:   9.77%,   9.85%,  10.06%,   9.70%,  10.01%,  10.39%,  10.22%,   9.79%,  10.28%,   9.93%
    Statistical Distance: 0.0096285553
    Kullback Distance: 0.0003629598
        Minimum Entropy: 3.2661763926 bits
    Digit 17:  33.70%,   1.19%,   7.61%,   9.44%,  10.17%,  10.23%,   9.73%,   9.27%,   7.48%,   1.17%
    Statistical Distance: 0.2410522923
    Kullback Distance: 0.4407810824
        Minimum Entropy: 1.5690094603 bits
    Digit 18:  97.71%,   0.10%,   0.17%,   0.30%,   0.40%,   0.39%,   0.35%,   0.25%,   0.20%,   0.11%
    Statistical Distance: 0.8771170736
    Kullback Distance: 3.0952252202
        Minimum Entropy: 0.0333966654 bits
    Digit 19:  99.60%,   0.04%,   0.04%,   0.05%,   0.06%,   0.04%,   0.05%,   0.07%,   0.05%,   0.01%
    Statistical Distance: 0.8959713158
    Kullback Distance: 3.2717776812
        Minimum Entropy: 0.0058239020 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.00%, 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.19%,  73.99%,  25.82%,   0.00%,   0.00%
    Statistical Distance: 0.4481468053
    Kullback Distance: 2.4791605137
        Minimum Entropy: 0.4345819127 bits
    Digit  2:  12.33%,   8.32%,   7.52%,   8.92%,   9.71%,   8.79%,   9.05%,  10.03%,  11.37%,  13.97%
    Statistical Distance: 0.0769962130
    Kullback Distance: 0.0247967389
        Minimum Entropy: 2.8394435597 bits
    Digit  3:   9.95%,  10.07%,   9.70%,   9.93%,   9.77%,   9.74%,  10.17%,   9.95%,  10.18%,  10.54%
    Statistical Distance: 0.0096366127
    Kullback Distance: 0.0004155157
        Minimum Entropy: 3.2461849175 bits
    Digit  4:  10.44%,  10.02%,   9.81%,  10.31%,  10.24%,   9.66%,   9.57%,   9.77%,  10.42%,   9.76%
    Statistical Distance: 0.0142212553
    Kullback Distance: 0.0006949402
        Minimum Entropy: 3.2594817401 bits
    Digit  5:   9.74%,  10.18%,   9.68%,   9.87%,   9.91%,  10.27%,  10.23%,   9.70%,   9.92%,  10.49%
    Statistical Distance: 0.0118121022
    Kullback Distance: 0.0005001550
        Minimum Entropy: 3.2528180098 bits
    Digit  6:  10.10%,  10.01%,  10.10%,  10.53%,  10.40%,   9.80%,   9.93%,   9.30%,  10.14%,   9.70%
    Statistical Distance: 0.0127628716
    Kullback Distance: 0.0008052721
        Minimum Entropy: 3.2472883172 bits
    Digit  7:  10.34%,  10.18%,  10.03%,   9.93%,  10.26%,  10.06%,   9.67%,  10.10%,   9.51%,   9.92%
    Statistical Distance: 0.0097010716
    Kullback Distance: 0.0004288416
        Minimum Entropy: 3.2740262879 bits
    Digit  8:  10.35%,  10.35%,   9.83%,   9.39%,  10.07%,  10.13%,  10.24%,  10.05%,   9.89%,   9.70%
    Statistical Distance: 0.0117959874
    Kullback Distance: 0.0005928399
        Minimum Entropy: 3.2729022558 bits
    Digit  9:   9.65%,  10.47%,  10.48%,   9.69%,   9.89%,   9.89%,  10.06%,   9.88%,   9.83%,  10.14%
    Statistical Distance: 0.0116509548
    Kullback Distance: 0.0005489998
        Minimum Entropy: 3.2539264962 bits
    Digit 10:  10.48%,   9.91%,  10.17%,   9.89%,   9.91%,  10.19%,   9.48%,  10.27%,   9.88%,   9.82%
    Statistical Distance: 0.0110869390
    Kullback Distance: 0.0005130307
        Minimum Entropy: 3.2539264962 bits
    Digit 11:   9.85%,  10.25%,  10.20%,   9.81%,   9.91%,   9.51%,  10.04%,   9.75%,  10.16%,  10.53%
    Statistical Distance: 0.0118040448
    Kullback Distance: 0.0005664177
        Minimum Entropy: 3.2472883172 bits
    Digit 12:   9.50%,  10.01%,   9.89%,  10.40%,  10.27%,  10.08%,   9.88%,   9.89%,  10.11%,   9.98%
    Statistical Distance: 0.0087422448
    Kullback Distance: 0.0003955940
        Minimum Entropy: 3.2650584576 bits
    Digit 13:  10.27%,  10.27%,   9.75%,  10.06%,   9.98%,   9.80%,  10.14%,  10.30%,   9.81%,   9.65%
    Statistical Distance: 0.0101925711
    Kullback Distance: 0.0003704081
        Minimum Entropy: 3.2796596220 bits
    Digit 14:  10.35%,   9.87%,   9.87%,   9.48%,  10.05%,  10.16%,   9.94%,   9.91%,  10.55%,   9.83%
    Statistical Distance: 0.0110063653
    Kullback Distance: 0.0005729001
        Minimum Entropy: 3.2450823610 bits
    Digit 15:  10.18%,   9.77%,   9.64%,  10.21%,  10.06%,  10.06%,   9.77%,  10.03%,  10.14%,  10.16%
    Statistical Distance: 0.0082426879
    Kullback Distance: 0.0002625684
        Minimum Entropy: 3.2921309338 bits
    Digit 16:  27.95%,   1.15%,  10.15%,  10.03%,  10.40%,  10.14%,  10.18%,   9.02%,   9.77%,   1.19%
    Statistical Distance: 0.1885746515
    Kullback Distance: 0.3386319069
        Minimum Entropy: 1.8390276177 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%, 100.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%, 100.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  3:   7.20%,  54.85%,  36.11%,   1.84%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4096768995
    Kullback Distance: 1.9369422437
        Minimum Entropy: 0.8663164213 bits
    Digit  4:   8.29%,   9.72%,   9.50%,  11.80%,  10.39%,  10.25%,  10.31%,  10.06%,   9.82%,   9.86%
    Statistical Distance: 0.0280799291
    Kullback Distance: 0.0049586184
        Minimum Entropy: 3.0826467936 bits
    Digit  5:  10.58%,  10.39%,  10.04%,   9.98%,  10.32%,   9.33%,   9.78%,   9.56%,  10.01%,  10.02%
    Statistical Distance: 0.0135686085
    Kullback Distance: 0.0009292597
        Minimum Entropy: 3.2406805421 bits
    Digit  6:  10.25%,  11.17%,  10.89%,   9.33%,   8.77%,   9.67%,  10.88%,  10.18%,   9.75%,   9.10%
    Statistical Distance: 0.0337200870
    Kullback Distance: 0.0042984143
        Minimum Entropy: 3.1626202008 bits
    Digit  7:  11.00%,   9.39%,  10.24%,   9.26%,  10.43%,   9.78%,   9.91%,   9.99%,  10.10%,   9.90%
    Statistical Distance: 0.0176939811
    Kullback Distance: 0.0016063824
        Minimum Entropy: 3.1846465071 bits
    Digit  8:  11.95%,  12.14%,  12.64%,  12.09%,  12.01%,   9.62%,   6.45%,   6.42%,   6.63%,  10.06%
    Statistical Distance: 0.1088067037
    Kullback Distance: 0.0464195322
        Minimum Entropy: 2.9837021060 bits
    Digit  9:   9.95%,   9.48%,  10.67%,   9.18%,  10.44%,   9.81%,  11.03%,   8.99%,  10.89%,   9.55%
    Statistical Distance: 0.0303440496
    Kullback Distance: 0.0033879556
        Minimum Entropy: 3.1804250117 bits
    Digit 10:  10.67%,  10.01%,   9.54%,   9.89%,   9.35%,  10.23%,   9.97%,  10.51%,   9.93%,   9.91%
    Statistical Distance: 0.0141487390
    Kullback Distance: 0.0010180249
        Minimum Entropy: 3.2286443361 bits
    Digit 11:  10.29%,  10.02%,   9.84%,  10.10%,   9.52%,  10.78%,   9.84%,  10.52%,   9.77%,   9.33%
    Statistical Distance: 0.0171219080
    Kullback Distance: 0.0012702246
        Minimum Entropy: 3.2134693423 bits
    Digit 12:  11.36%,   6.70%,   7.21%,  10.90%,  12.35%,  11.92%,   7.05%,   8.49%,  12.37%,  11.64%
    Statistical Distance: 0.1054226090
    Kullback Distance: 0.0374907876
        Minimum Entropy: 3.0153088027 bits
    Digit 13:   9.56%,  10.25%,  10.02%,  10.47%,   9.31%,  10.54%,  10.05%,  10.18%,  10.11%,   9.51%
    Statistical Distance: 0.0162194827
    Kullback Distance: 0.0011159528
        Minimum Entropy: 3.2461849175 bits
    Digit 14:  16.61%,   5.87%,  11.00%,   9.35%,   9.22%,  11.09%,   9.45%,   9.66%,  11.31%,   6.43%
    Statistical Distance: 0.1001208605
    Kullback Distance: 0.0550182607
        Minimum Entropy: 2.5895031256 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Wrote 137494 bytes (1099952 bits) to file: "Data/2018_07_17_18_54_40_gravity.csv.bin"
    Generated 1099952 bits of entropy from data collected over 16m 49s

    Parsing file: Data/2018_07_17_19_17_00_gyroscope.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  97.62%,   2.36%,   0.02%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5262122598
    Kullback Distance: 3.1583884347
        Minimum Entropy: 0.0347332248 bits
    Digit  3:  49.04%,   6.13%,   0.02%,   0.00%,  15.23%,  22.86%,   5.98%,   0.00%,   0.00%,   0.75%
    Statistical Distance: 0.4213022263
    Kullback Distance: 1.3730031577
        Minimum Entropy: 1.0279884952 bits
    Digit  4:  14.93%,  12.20%,  11.36%,  13.24%,   6.39%,   6.16%,  16.99%,   6.34%,   4.21%,   8.19%
    Statistical Distance: 0.1870997255
    Kullback Distance: 0.1233938958
        Minimum Entropy: 2.5575066691 bits
    Digit  5:   9.41%,  11.18%,  10.31%,   8.86%,   9.76%,  10.52%,  11.39%,   9.50%,   8.71%,  10.37%
    Statistical Distance: 0.0376639219
    Kullback Distance: 0.0053761815
        Minimum Entropy: 3.1340757536 bits
    Digit  6:  13.27%,   8.83%,   8.26%,   7.14%,  10.40%,  10.93%,  10.89%,   9.73%,   8.84%,  11.71%
    Statistical Distance: 0.0719731625
    Kullback Distance: 0.0212234310
        Minimum Entropy: 2.9141685957 bits
    Digit  7:  10.43%,   9.32%,   9.42%,   8.51%,   8.63%,  12.05%,  10.25%,  11.89%,  10.29%,   9.21%
    Statistical Distance: 0.0491003355
    Kullback Distance: 0.0096685396
        Minimum Entropy: 3.0533313433 bits
    Digit  8:   8.25%,   9.56%,  10.29%,   9.29%,  11.15%,  10.31%,  10.66%,  10.03%,   9.15%,  11.31%
    Statistical Distance: 0.0375419335
    Kullback Distance: 0.0059527883
        Minimum Entropy: 3.1437648098 bits
    Digit  9:   8.98%,   8.69%,  11.16%,   8.92%,   9.53%,  10.28%,  11.88%,   9.53%,   9.91%,  11.12%
    Statistical Distance: 0.0443427874
    Kullback Distance: 0.0075295359
        Minimum Entropy: 3.0735606683 bits
    Digit 10:   9.76%,   8.55%,   9.65%,   8.91%,  12.08%,  10.37%,  10.17%,  10.49%,  10.57%,   9.45%
    Statistical Distance: 0.0367490088
    Kullback Distance: 0.0062660934
        Minimum Entropy: 3.0496835663 bits
    Digit 11:   9.36%,  12.00%,   8.48%,  10.66%,  12.11%,   8.87%,   9.64%,   9.07%,  10.46%,   9.35%
    Statistical Distance: 0.0522720342
    Kullback Distance: 0.0102305865
        Minimum Entropy: 3.0460449893 bits
    Digit 12:  12.02%,  10.26%,   9.16%,   7.78%,   8.87%,  10.25%,  10.32%,   9.52%,  10.03%,  11.79%
    Statistical Distance: 0.0466910643
    Kullback Distance: 0.0106023357
        Minimum Entropy: 3.0569883669 bits
    Digit 13:  13.25%,   9.71%,   9.00%,   8.19%,   9.16%,   8.54%,   9.03%,  11.91%,  11.12%,  10.09%
    Statistical Distance: 0.0637084477
    Kullback Distance: 0.0164410003
        Minimum Entropy: 2.9158278196 bits
    Digit 14:  12.70%,  10.45%,   9.13%,  12.63%,   9.77%,   9.97%,  10.05%,   9.62%,   6.83%,   8.84%
    Statistical Distance: 0.0582189692
    Kullback Distance: 0.0194386162
        Minimum Entropy: 2.9768675010 bits
    Digit 15:   9.71%,   8.83%,  10.57%,   9.35%,  10.98%,  11.51%,  12.09%,   8.78%,   9.71%,   8.46%
    Statistical Distance: 0.0515096066
    Kullback Distance: 0.0098056880
        Minimum Entropy: 3.0478631307 bits
    Digit 16:   9.76%,   9.33%,  11.47%,   9.38%,   8.72%,  12.03%,  10.81%,   8.20%,   9.77%,  10.52%
    Statistical Distance: 0.0483074108
    Kullback Distance: 0.0093353081
        Minimum Entropy: 3.0551586964 bits
    Digit 17:  11.63%,   9.85%,   9.29%,  10.61%,  10.05%,   6.60%,   9.58%,   9.44%,  12.50%,  10.45%
    Statistical Distance: 0.0524550168
    Kullback Distance: 0.0166515748
        Minimum Entropy: 2.9995600869 bits
    Digit 18:  11.85%,   7.49%,  10.08%,  10.19%,   9.90%,   9.26%,  12.93%,  11.25%,   8.22%,   8.84%
    Statistical Distance: 0.0629765172
    Kullback Distance: 0.0179607331
        Minimum Entropy: 2.9511197318 bits
    Digit 19:  49.45%,   4.15%,   7.40%,   6.71%,   4.88%,   5.25%,   7.17%,   6.22%,   3.95%,   4.83%
    Statistical Distance: 0.3945105215
    Kullback Distance: 0.7368523596
        Minimum Entropy: 1.0159268779 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  98.64%,   1.34%,   0.02%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5364287893
    Kullback Distance: 3.2170898494
        Minimum Entropy: 0.0197131890 bits
    Digit  3:   0.96%,  20.88%,  29.43%,  24.35%,   3.55%,   0.29%,   3.72%,   8.20%,   7.06%,   1.56%
    Statistical Distance: 0.4465690759
    Kullback Distance: 0.7386081994
        Minimum Entropy: 1.7646550542 bits
    Digit  4:   9.91%,   9.15%,   9.87%,  12.34%,   9.59%,   9.32%,   9.93%,   8.10%,  11.36%,  10.45%
    Statistical Distance: 0.0414150656
    Kullback Distance: 0.0089031765
        Minimum Entropy: 3.0190442940 bits
    Digit  5:  11.67%,   7.90%,   9.74%,   9.91%,   9.68%,  10.40%,  10.70%,  10.99%,   9.16%,   9.84%
    Statistical Distance: 0.0376334248
    Kullback Distance: 0.0071511279
        Minimum Entropy: 3.0997242488 bits
    Digit  6:   9.61%,  10.14%,   8.95%,   9.77%,   9.42%,  11.01%,   8.65%,  11.48%,  10.46%,  10.51%
    Statistical Distance: 0.0359865813
    Kullback Distance: 0.0051743227
        Minimum Entropy: 3.1225341317 bits
    Digit  7:  10.11%,  11.38%,   9.70%,   8.69%,  11.80%,   8.78%,  11.13%,  10.45%,  10.13%,   7.84%
    Statistical Distance: 0.0498932601
    Kullback Distance: 0.0106903157
        Minimum Entropy: 3.0828504302 bits
    Digit  8:  11.31%,   9.48%,   9.06%,  12.02%,   8.81%,  10.70%,  10.58%,   9.47%,  10.66%,   7.90%
    Statistical Distance: 0.0527599878
    Kullback Distance: 0.0103998250
        Minimum Entropy: 3.0569883669 bits
    Digit  9:   9.97%,   9.50%,   9.15%,   9.41%,  11.06%,  10.70%,  10.51%,  10.77%,   9.24%,   9.70%
    Statistical Distance: 0.0303141202
    Kullback Distance: 0.0031931038
        Minimum Entropy: 3.1772030015 bits
    Digit 10:   9.91%,   9.79%,   9.00%,   9.45%,  11.65%,   9.68%,   8.94%,  11.47%,  11.68%,   8.43%
    Statistical Distance: 0.0479719427
    Kullback Distance: 0.0090795710
        Minimum Entropy: 3.0978396045 bits
    Digit 11:   9.50%,   9.97%,  10.02%,  10.16%,   9.52%,  10.70%,   9.90%,   9.93%,  10.23%,  10.08%
    Statistical Distance: 0.0118938701
    Kullback Distance: 0.0007786153
        Minimum Entropy: 3.2237129661 bits
    Digit 12:   9.74%,  10.05%,  10.06%,   9.93%,  10.09%,  10.93%,   9.52%,  10.70%,   9.24%,   9.73%
    Statistical Distance: 0.0184507472
    Kullback Distance: 0.0016750962
        Minimum Entropy: 3.1932108777 bits
    Digit 13:   9.59%,   9.47%,   9.88%,  10.80%,   9.23%,   9.80%,   9.70%,   9.38%,  10.78%,  11.38%
    Statistical Distance: 0.0295211955
    Kullback Distance: 0.0033430195
        Minimum Entropy: 3.1360083661 bits
    Digit 14:  11.13%,   9.01%,  10.32%,   8.75%,  11.21%,   8.58%,  10.89%,  10.90%,   9.26%,   9.94%
    Statistical Distance: 0.0445257701
    Kullback Distance: 0.0069308922
        Minimum Entropy: 3.1574397467 bits
    Digit 15:   7.82%,  11.56%,   9.10%,  10.49%,  10.45%,   8.78%,  11.45%,   9.19%,   9.90%,  11.25%
    Statistical Distance: 0.0519975602
    Kullback Distance: 0.0104200508
        Minimum Entropy: 3.1129861482 bits
    Digit 16:  10.61%,  10.31%,   8.58%,  10.14%,  11.24%,  10.17%,   9.53%,   9.47%,   9.16%,  10.78%
    Statistical Distance: 0.0325099116
    Kullback Distance: 0.0042646825
        Minimum Entropy: 3.1535193773 bits
    Digit 17:  10.26%,  11.19%,  10.16%,  11.48%,   8.45%,  10.29%,   9.07%,   9.45%,   9.55%,  10.09%
    Statistical Distance: 0.0347971943
    Kullback Distance: 0.0054793060
        Minimum Entropy: 3.1225341317 bits
    Digit 18:  11.77%,   7.94%,   9.23%,  11.44%,  10.93%,  10.60%,  10.81%,  10.72%,   7.64%,   8.92%
    Statistical Distance: 0.0627020433
    Kullback Distance: 0.0142305803
        Minimum Entropy: 3.0865831491 bits
    Digit 19:  54.68%,   1.42%,   2.65%,   7.88%,   7.87%,   7.03%,   6.98%,   6.66%,   4.13%,   0.69%
    Statistical Distance: 0.4468130528
    Kullback Distance: 1.0051056454
        Minimum Entropy: 0.8708804135 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  98.15%,   1.85%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4815492528
    Kullback Distance: 3.1892768176
        Minimum Entropy: 0.0268674329 bits
    Digit  3:  26.33%,  25.02%,   0.11%,   6.31%,  18.16%,  12.21%,   9.94%,   0.00%,   0.00%,   1.91%
    Statistical Distance: 0.3173223544
    Kullback Distance: 0.7952708479
        Minimum Entropy: 1.9249878189 bits
    Digit  4:  11.89%,  12.52%,  13.05%,  13.74%,  11.36%,   7.41%,   6.59%,   6.89%,   6.56%,   9.99%
    Statistical Distance: 0.1256480634
    Kullback Distance: 0.0555528233
        Minimum Entropy: 2.8636568906 bits
    Digit  5:  10.86%,   9.23%,  11.76%,  10.00%,   9.74%,   9.68%,   8.86%,   9.85%,   9.50%,  10.52%
    Statistical Distance: 0.0313815188
    Kullback Distance: 0.0045556249
        Minimum Entropy: 3.0884531365 bits
    Digit  6:  10.06%,   8.49%,  10.03%,   9.24%,  11.34%,  10.09%,  10.73%,   8.58%,  10.63%,  10.78%
    Statistical Distance: 0.0368100030
    Kullback Distance: 0.0060192195
        Minimum Entropy: 3.1398813753 bits
    Digit  7:  11.06%,   9.87%,   9.44%,  11.47%,   9.68%,   8.94%,  11.91%,   7.62%,   9.52%,  10.51%
    Statistical Distance: 0.0493748094
    Kullback Distance: 0.0106924427
        Minimum Entropy: 3.0698614483 bits
    Digit  8:   8.91%,   8.52%,  11.53%,  10.95%,   9.45%,  11.04%,   9.27%,   9.79%,   9.71%,  10.83%
    Statistical Distance: 0.0434278744
    Kullback Distance: 0.0067248474
        Minimum Entropy: 3.1167977622 bits
    Digit  9:   9.80%,   8.62%,   8.94%,  10.34%,  10.25%,  11.45%,  10.73%,   9.52%,  10.19%,  10.17%
    Statistical Distance: 0.0312900274
    Kullback Distance: 0.0045024487
        Minimum Entropy: 3.1263710889 bits
    Digit 10:  10.67%,  11.67%,   9.45%,   9.84%,  11.62%,   8.81%,   8.84%,  10.60%,   9.76%,   8.74%
    Statistical Distance: 0.0455626715
    Kullback Distance: 0.0078114461
        Minimum Entropy: 3.0997242488 bits
    Digit 11:  10.09%,   9.94%,   9.23%,  10.17%,   9.53%,  10.93%,   8.94%,  10.40%,  11.01%,   9.76%
    Statistical Distance: 0.0260750229
    Kullback Distance: 0.0029605908
        Minimum Entropy: 3.1831851595 bits
    Digit 12:   9.04%,   9.24%,   9.96%,  10.41%,  10.48%,  11.56%,  10.23%,   9.84%,   8.52%,  10.72%
    Statistical Distance: 0.0340042696
    Kullback Distance: 0.0051407960
        Minimum Entropy: 3.1129861482 bits
    Digit 13:   9.79%,   9.67%,   9.45%,  12.02%,  11.83%,   9.42%,   8.05%,  10.60%,  10.13%,   9.04%
    Statistical Distance: 0.0457151571
    Kullback Distance: 0.0095004832
        Minimum Entropy: 3.0569883669 bits
    Digit 14:   9.10%,   8.86%,  10.58%,  11.82%,   9.61%,  10.98%,   9.71%,   8.49%,  11.51%,   9.33%
    Statistical Distance: 0.0489173529
    Kullback Distance: 0.0085364652
        Minimum Entropy: 3.0809876862 bits
    Digit 15:   9.39%,   9.68%,   8.25%,   9.10%,  11.21%,  13.02%,  11.42%,   9.07%,   8.97%,   9.88%
    Statistical Distance: 0.0565111314
    Kullback Distance: 0.0131588834
        Minimum Entropy: 2.9409479268 bits
    Digit 16:  10.54%,  10.60%,   8.54%,   9.76%,  10.49%,  11.54%,  10.60%,   7.70%,   8.91%,  11.33%
    Statistical Distance: 0.0509606587
    Kullback Distance: 0.0104450421
        Minimum Entropy: 3.1148906964 bits
    Digit 17:  11.19%,  10.66%,  10.54%,   8.23%,   9.44%,   9.07%,  10.35%,   9.96%,   9.99%,  10.57%
    Statistical Distance: 0.0330893565
    Kullback Distance: 0.0050825127
        Minimum Entropy: 3.1594039335 bits
    Digit 18:  12.24%,   7.64%,  11.57%,   7.37%,  11.51%,  11.06%,   9.59%,  11.01%,   9.03%,   8.98%
    Statistical Distance: 0.0739554742
    Kullback Distance: 0.0196493254
        Minimum Entropy: 3.0297840089 bits
    Digit 19:  45.84%,   3.05%,   5.35%,   8.10%,   8.30%,   6.15%,   6.16%,   6.88%,   6.07%,   4.12%
    Statistical Distance: 0.3583714547
    Kullback Distance: 0.6394732018
        Minimum Entropy: 1.1254108925 bits

    Wrote 84808 bytes (678464 bits) to file: "Data/2018_07_17_19_17_00_gyroscope.csv.bin"
    Generated 678464 bits of entropy from data collected over 4m 23s

    Parsing file: Data/2018_07_17_19_22_55_gyroscopeuncalibrated.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  83.60%,  16.40%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.6782317679
        Minimum Entropy: 0.2583537807 bits
    Digit  3:  43.96%,   0.04%,   0.00%,   0.00%,   0.15%,  55.79%,   0.04%,   0.00%,   0.00%,   0.01%
    Statistical Distance: 0.5974889217
    Kullback Distance: 2.3053264345
        Minimum Entropy: 0.8419150500 bits
    Digit  4:  28.40%,   0.03%,   0.06%,  54.73%,   0.13%,   0.06%,  15.49%,   0.09%,   0.04%,   0.96%
    Statistical Distance: 0.6862629247
    Kullback Distance: 1.8062589272
        Minimum Entropy: 0.8696822913 bits
    Digit  5:  28.42%,   0.09%,  54.73%,   0.04%,   0.07%,  15.51%,   0.06%,   0.96%,   0.07%,   0.04%
    Statistical Distance: 0.6865583456
    Kullback Distance: 1.8082198191
        Minimum Entropy: 0.8696822913 bits
    Digit  6:  28.36%,   0.07%,  15.57%,   0.07%,   0.12%,   0.04%,  54.76%,   0.06%,   0.93%,   0.01%
    Statistical Distance: 0.6868537666
    Kullback Distance: 1.8099877441
        Minimum Entropy: 0.8689037186 bits
    Digit  7:  28.39%,   0.07%,   0.12%,  54.76%,   0.04%,   0.03%,  15.48%,   0.04%,   0.03%,   1.03%
    Statistical Distance: 0.6862629247
    Kullback Distance: 1.8096104408
        Minimum Entropy: 0.8689037186 bits
    Digit  8:  28.40%,   0.10%,  54.70%,   0.06%,  15.49%,   0.13%,   0.97%,   0.01%,   0.06%,   0.06%
    Statistical Distance: 0.6859675037
    Kullback Distance: 1.8042305371
        Minimum Entropy: 0.8704612843 bits
    Digit  9:  28.51%,   0.24%,  54.51%,   0.07%,  15.47%,   0.04%,   0.93%,   0.06%,   0.10%,   0.07%
    Statistical Distance: 0.6847858198
    Kullback Distance: 1.7917906783
        Minimum Entropy: 0.8755350175 bits
    Digit 10:  28.40%,   0.09%,  54.27%,   0.06%,   0.01%,  15.58%,   0.28%,   0.06%,   1.20%,   0.04%
    Statistical Distance: 0.6825701625
    Kullback Distance: 1.7810146141
        Minimum Entropy: 0.8818042074 bits
    Digit 11:  28.38%,   0.06%,   0.07%,   0.93%,   0.09%,  15.47%,   0.34%,  54.34%,   0.06%,   0.27%
    Statistical Distance: 0.6818316100
    Kullback Distance: 1.7689282726
        Minimum Entropy: 0.8798421575 bits
    Digit 12:  28.45%,   0.37%,   0.04%,   0.09%,   0.01%,   0.09%,   0.00%,   0.27%,   0.06%,  70.62%
    Statistical Distance: 0.7406942393
    Kullback Distance: 2.3678736097
        Minimum Entropy: 0.5018434281 bits
    Digit 13:  28.40%,   0.06%,   0.09%,   0.03%,   0.09%,   0.09%,   0.96%,  15.70%,  54.22%,   0.35%
    Statistical Distance: 0.6833087149
    Kullback Distance: 1.7780002714
        Minimum Entropy: 0.8829827194 bits
    Digit 14:  28.43%,   1.00%,   0.00%,   0.07%,  15.78%,   0.07%,   0.04%,  54.22%,   0.10%,   0.27%
    Statistical Distance: 0.6343426883
    Kullback Distance: 1.7869832348
        Minimum Entropy: 0.8829827194 bits
    Digit 15:  82.56%,  16.40%,   0.09%,   0.28%,   0.09%,   0.04%,   0.04%,   0.40%,   0.04%,   0.06%
    Statistical Distance: 0.7895125554
    Kullback Distance: 2.5712100517
        Minimum Entropy: 0.2765656588 bits
    Digit 16:  43.90%,   0.31%,   0.04%,   0.04%,   0.06%,  55.18%,   0.06%,   0.03%,   0.09%,   0.28%
    Statistical Distance: 0.7908419498
    Kullback Distance: 2.2425914176
        Minimum Entropy: 0.8576613788 bits
    Digit 17:  28.58%,  54.62%,  15.47%,   0.97%,   0.06%,   0.03%,   0.06%,   0.10%,   0.04%,   0.06%
    Statistical Distance: 0.6867060561
    Kullback Distance: 1.8097059125
        Minimum Entropy: 0.8724106091 bits
    Digit 18:  98.91%,   0.27%,   0.01%,   0.06%,   0.06%,   0.03%,   0.07%,   0.10%,   0.13%,   0.35%
    Statistical Distance: 0.8890694239
    Kullback Distance: 3.2059855257
        Minimum Entropy: 0.0158563059 bits
    Digit 19:  99.81%,   0.01%,   0.01%,   0.01%,   0.00%,   0.03%,   0.03%,   0.04%,   0.01%,   0.03%
    Statistical Distance: 0.8480797637
    Kullback Distance: 3.2963141583
        Minimum Entropy: 0.0027729787 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  14.05%,  78.36%,   7.59%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.3740768095
    Kullback Distance: 2.3661035967
        Minimum Entropy: 0.3518030827 bits
    Digit  3:  43.77%,   7.31%,   0.00%,   0.00%,   0.03%,  48.45%,   0.40%,   0.03%,   0.00%,   0.01%
    Statistical Distance: 0.5721565731
    Kullback Distance: 1.9771617088
        Minimum Entropy: 1.0454600190 bits
    Digit  4:   1.09%,   0.01%,   0.10%,  20.16%,   0.07%,   0.06%,  42.95%,   0.04%,   0.07%,  35.42%
    Statistical Distance: 0.6853766617
    Kullback Distance: 1.6920978110
        Minimum Entropy: 1.2191285645 bits
    Digit  5:   8.38%,   0.06%,  12.98%,   0.38%,   0.06%,  42.57%,   0.10%,  35.42%,   0.03%,   0.01%
    Statistical Distance: 0.6097488922
    Kullback Distance: 1.5259291094
        Minimum Entropy: 1.2320854983 bits
    Digit  6:   1.15%,   0.38%,  42.58%,   0.06%,   0.10%,   7.28%,  12.98%,   0.03%,  35.38%,   0.04%
    Statistical Distance: 0.6094534712
    Kullback Distance: 1.4794863406
        Minimum Entropy: 1.2315849970 bits
    Digit  7:   1.18%,   0.04%,   7.30%,  13.01%,   0.07%,   0.03%,  42.91%,   0.03%,   0.04%,  35.38%
    Statistical Distance: 0.6129985229
    Kullback Distance: 1.5092568121
        Minimum Entropy: 1.2206176702 bits
    Digit  8:   1.11%,   0.40%,  13.00%,   0.04%,  42.63%,   0.09%,  35.39%,   0.06%,   7.28%,   0.00%
    Statistical Distance: 0.5601920236
    Kullback Distance: 1.4853402980
        Minimum Entropy: 1.2300845340 bits
    Digit  9:   1.06%,   0.38%,  12.95%,   0.07%,  42.56%,   0.07%,  35.47%,   0.07%,   0.06%,   7.30%
    Statistical Distance: 0.6097488922
    Kullback Distance: 1.4795593979
        Minimum Entropy: 1.2325861733 bits
    Digit 10:   1.11%,   7.27%,  12.98%,   0.38%,   0.07%,  42.58%,   0.07%,   0.07%,  35.41%,   0.04%
    Statistical Distance: 0.6097488922
    Kullback Distance: 1.4790474935
        Minimum Entropy: 1.2315849970 bits
    Digit 11:   1.05%,   7.34%,   0.06%,  35.41%,   0.06%,  42.58%,   0.10%,  12.95%,   0.07%,   0.37%
    Statistical Distance: 0.6094534712
    Kullback Distance: 1.4791293201
        Minimum Entropy: 1.2315849970 bits
    Digit 12:   1.06%,   0.03%,   0.07%,   0.03%,   0.07%,   0.15%,   0.04%,   0.07%,   0.10%,  98.36%
    Statistical Distance: 0.8836041359
    Kullback Distance: 3.1696979200
        Minimum Entropy: 0.0238502936 bits
    Digit 13:   1.11%,   0.01%,   0.07%,   0.43%,   7.34%,   0.03%,  35.36%,  42.58%,  12.97%,   0.09%
    Statistical Distance: 0.6091580502
    Kullback Distance: 1.4806634742
        Minimum Entropy: 1.2315849970 bits
    Digit 14:   1.12%,  35.38%,   0.12%,   0.09%,  42.60%,   0.35%,   0.06%,  12.97%,   7.25%,   0.06%
    Statistical Distance: 0.6094534712
    Kullback Distance: 1.4757132945
        Minimum Entropy: 1.2310846692 bits
    Digit 15:  14.02%,  77.95%,   7.61%,   0.13%,   0.07%,   0.04%,   0.04%,   0.06%,   0.04%,   0.03%
    Statistical Distance: 0.7196454948
    Kullback Distance: 2.3166867410
        Minimum Entropy: 0.3594378503 bits
    Digit 16:  50.86%,   0.10%,   0.07%,   0.06%,   0.06%,  48.63%,   0.06%,   0.07%,   0.03%,   0.06%
    Statistical Distance: 0.7948301329
    Kullback Distance: 2.2655878548
        Minimum Entropy: 0.9754896529 bits
    Digit 17:   1.14%,  12.94%,  42.53%,  35.44%,   7.30%,   0.35%,   0.07%,   0.04%,   0.12%,   0.07%
    Statistical Distance: 0.6090103397
    Kullback Distance: 1.4755491245
        Minimum Entropy: 1.2335880449 bits
    Digit 18:  99.48%,   0.03%,   0.04%,   0.10%,   0.01%,   0.09%,   0.07%,   0.07%,   0.03%,   0.06%
    Statistical Distance: 0.8948301329
    Kullback Distance: 3.2598009084
        Minimum Entropy: 0.0074778881 bits
    Digit 19:  99.99%,   0.00%,   0.01%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4998522895
    Kullback Distance: 3.3198354023
        Minimum Entropy: 0.0002131169 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:  90.89%,   9.11%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4088626292
    Kullback Distance: 2.8816732332
        Minimum Entropy: 0.1378658413 bits
    Digit  3:  48.88%,   0.03%,   0.00%,   0.00%,   0.13%,  50.92%,   0.04%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5479320532
    Kullback Distance: 2.3002134294
        Minimum Entropy: 0.9738145351 bits
    Digit  4:  39.94%,   0.06%,   0.10%,  50.52%,   0.06%,   0.04%,   8.80%,   0.04%,   0.07%,   0.35%
    Statistical Distance: 0.7045790251
    Kullback Distance: 1.9174402836
        Minimum Entropy: 0.9851595087 bits
    Digit  5:  39.91%,   0.07%,  50.47%,   0.07%,   0.07%,   8.82%,   0.07%,   0.40%,   0.03%,   0.07%
    Statistical Distance: 0.7038404727
    Kullback Distance: 1.9125734635
        Minimum Entropy: 0.9864255860 bits
    Digit  6:  39.96%,   0.06%,   8.82%,   0.03%,   0.07%,   0.06%,  50.52%,   0.06%,   0.37%,   0.06%
    Statistical Distance: 0.7047267356
    Kullback Distance: 1.9201585937
        Minimum Entropy: 0.9851595087 bits
    Digit  7:  39.90%,   0.04%,   0.06%,  50.47%,   0.07%,   0.06%,   8.85%,   0.06%,   0.07%,   0.41%
    Statistical Distance: 0.7036927622
    Kullback Distance: 1.9135544227
        Minimum Entropy: 0.9864255860 bits
    Digit  8:  39.94%,   0.06%,  50.49%,   0.04%,   8.80%,   0.06%,   0.41%,   0.04%,   0.09%,   0.06%
    Statistical Distance: 0.7042836041
    Kullback Distance: 1.9160298523
        Minimum Entropy: 0.9860034368 bits
    Digit  9:  39.88%,   0.07%,  50.49%,   0.10%,   8.83%,   0.06%,   0.37%,   0.06%,   0.06%,   0.07%
    Statistical Distance: 0.7036927622
    Kullback Distance: 1.9114951725
        Minimum Entropy: 0.9860034368 bits
    Digit 10:  39.87%,   0.09%,  50.47%,   0.10%,   0.03%,   8.91%,   0.06%,   0.06%,   0.38%,   0.03%
    Statistical Distance: 0.7033973412
    Kullback Distance: 1.9147073566
        Minimum Entropy: 0.9864255860 bits
    Digit 11:  39.94%,   0.03%,   0.04%,   0.41%,   0.06%,   8.85%,   0.04%,  50.50%,   0.10%,   0.01%
    Statistical Distance: 0.7044313146
    Kullback Distance: 1.9212469893
        Minimum Entropy: 0.9855814110 bits
    Digit 12:  39.93%,   0.07%,   0.06%,   0.07%,   0.09%,   0.04%,   0.03%,   0.07%,   0.07%,  59.56%
    Statistical Distance: 0.7948301329
    Kullback Distance: 2.2933292710
        Minimum Entropy: 0.7476601950 bits
    Digit 13:  39.88%,   0.06%,   0.04%,   0.09%,   0.06%,   0.09%,   0.38%,   8.83%,  50.52%,   0.04%
    Statistical Distance: 0.7039881832
    Kullback Distance: 1.9147651422
        Minimum Entropy: 0.9851595087 bits
    Digit 14:  39.88%,   0.38%,   0.01%,   0.01%,   8.86%,   0.09%,   0.06%,  50.47%,   0.15%,   0.07%
    Statistical Distance: 0.7035450517
    Kullback Distance: 1.9138200989
        Minimum Entropy: 0.9864255860 bits
    Digit 15:  90.30%,   9.13%,   0.04%,   0.13%,   0.06%,   0.06%,   0.09%,   0.04%,   0.06%,   0.09%
    Statistical Distance: 0.8029542097
    Kullback Distance: 2.8141353974
        Minimum Entropy: 0.1472752667 bits
    Digit 16:  48.64%,   0.03%,   0.06%,   0.03%,   0.07%,  50.83%,   0.07%,   0.12%,   0.06%,   0.09%
    Statistical Distance: 0.7946824225
    Kullback Distance: 2.2644951666
        Minimum Entropy: 0.9763279417 bits
    Digit 17:  39.87%,  50.47%,   8.83%,   0.40%,   0.10%,   0.06%,   0.12%,   0.01%,   0.07%,   0.06%
    Statistical Distance: 0.7033973412
    Kullback Distance: 1.9101145706
        Minimum Entropy: 0.9864255860 bits
    Digit 18:  99.38%,   0.06%,   0.03%,   0.06%,   0.03%,   0.04%,   0.09%,   0.07%,   0.09%,   0.15%
    Statistical Distance: 0.8937961595
    Kullback Distance: 3.2489223382
        Minimum Entropy: 0.0089781282 bits
    Digit 19:  99.68%,   0.07%,   0.06%,   0.04%,   0.01%,   0.06%,   0.04%,   0.01%,   0.00%,   0.01%
    Statistical Distance: 0.8467503693
    Kullback Distance: 3.2813790651
        Minimum Entropy: 0.0046958602 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 4m 32s

    Parsing file: Data/2018_07_17_19_28_02_magneticfield.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  10.99%,  22.82%,   0.00%,   2.04%,  19.24%,  10.87%,   4.92%,   0.09%,  26.79%,   2.24%
    Statistical Distance: 0.3570859977
    Kullback Distance: 0.7105257623
        Minimum Entropy: 1.9001756783 bits
    Digit  1:   0.20%,  27.14%,   0.00%,  21.48%,   0.00%,   0.09%,  21.66%,   1.68%,  27.74%,   0.00%
    Statistical Distance: 0.4302949237
    Kullback Distance: 1.2173910956
        Minimum Entropy: 1.8497677088 bits
    Digit  2:   0.29%,   0.00%,  48.81%,   0.00%,   0.00%,   1.68%,   0.00%,  49.22%,   0.00%,   0.00%
    Statistical Distance: 0.4802949237
    Kullback Distance: 2.1900619906
        Minimum Entropy: 1.0225796297 bits
    Digit  3:   1.97%,   0.00%,   0.00%,   0.00%,   0.00%,  98.03%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4802949237
    Kullback Distance: 3.1821466798
        Minimum Entropy: 0.0287122427 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   2.70%,   0.26%,   4.12%,  29.02%,   0.31%,  11.42%,  19.67%,   0.00%,   0.59%,  31.91%
    Statistical Distance: 0.4701357171
    Kullback Distance: 1.0368257822
        Minimum Entropy: 1.6480748405 bits
    Digit  1:   0.01%,  29.04%,   5.86%,  32.22%,   0.00%,   4.12%,  14.11%,   0.57%,  14.07%,   0.00%
    Statistical Distance: 0.3942972726
    Kullback Distance: 1.0068506543
        Minimum Entropy: 1.6339824692 bits
    Digit  2:   4.14%,   0.00%,  43.14%,   0.00%,   0.00%,   6.43%,   0.00%,  46.29%,   0.00%,   0.00%
    Statistical Distance: 0.3942972726
    Kullback Distance: 1.8395383702
        Minimum Entropy: 1.1113099754 bits
    Digit  3:  10.57%,   0.00%,   0.00%,   0.00%,   0.00%,  89.43%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.8351116577
        Minimum Entropy: 0.1611736187 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  17.67%,   1.94%,  18.83%,   3.26%,  11.97%,   7.32%,  16.91%,   0.03%,  22.07%,   0.00%
    Statistical Distance: 0.3244616991
    Kullback Distance: 0.5944029034
        Minimum Entropy: 2.1800326460 bits
    Digit  1:  14.77%,   7.26%,  13.73%,  11.97%,   0.00%,  18.86%,  10.22%,  18.07%,   5.13%,   0.00%
    Statistical Distance: 0.1761581626
    Kullback Distance: 0.4239866976
        Minimum Entropy: 2.4068398130 bits
    Digit  2:  33.63%,   0.00%,  17.47%,   0.00%,   0.00%,  31.80%,   0.00%,  17.10%,   0.00%,   0.00%
    Statistical Distance: 0.3000000000
    Kullback Distance: 1.3921688553
        Minimum Entropy: 1.5722167685 bits
    Digit  3:  65.43%,   0.00%,   0.00%,   0.00%,   0.00%,  34.57%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.3917709472
        Minimum Entropy: 0.6119473095 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 5m 7s

    Parsing file: Data/2018_07_17_19_34_12_magneticfielduncalibrated.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.05%,  26.98%,   5.49%,   1.92%,  34.39%,   1.03%,   0.16%,   9.96%,  19.83%,   0.19%
    Statistical Distance: 0.5120177695
    Kullback Distance: 1.0435354479
        Minimum Entropy: 1.5398019606 bits
    Digit  1:   0.07%,  26.96%,   0.14%,  15.45%,   0.00%,   1.03%,  21.60%,   0.19%,  34.53%,   0.02%
    Statistical Distance: 0.5355038578
    Kullback Distance: 1.2801132512
        Minimum Entropy: 1.5339293811 bits
    Digit  2:   1.10%,   0.00%,  48.56%,   0.00%,   0.05%,   0.28%,   0.00%,  49.99%,   0.00%,   0.02%
    Statistical Distance: 0.5855038578
    Kullback Distance: 2.2125193175
        Minimum Entropy: 1.0003373542 bits
    Digit  3:   1.38%,   0.00%,   0.00%,   0.00%,   0.14%,  98.41%,   0.00%,   0.00%,   0.00%,   0.07%
    Statistical Distance: 0.5841010054
    Kullback Distance: 3.1932816839
        Minimum Entropy: 0.0231216975 bits
    Digit  4:   0.00%,   0.00%,   0.00%,   0.00%,  99.79%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit  5:   0.00%,  99.79%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.21%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit  6:   0.00%,  99.79%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.21%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit  7:   0.21%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,  99.79%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit  8:   0.00%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,  99.79%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit  9:   0.00%,   0.00%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%,  99.79%,   0.00%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit 10:   0.00%,   0.00%,   0.00%,  99.79%,   0.00%,   0.00%,   0.21%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit 11:  99.79%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.21%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit 12:   0.00%,   0.00%,   0.00%,   0.00%,  99.79%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4978957213
    Kullback Distance: 3.3001832478
        Minimum Entropy: 0.0030390311 bits
    Digit 13:   0.00%,   0.00%,   0.00%,   0.21%,   0.00%,   0.00%,   0.14%,  99.65%,   0.00%,   0.00%
    Statistical Distance: 0.5464928688
    Kullback Distance: 3.2848696227
        Minimum Entropy: 0.0050686141 bits
    Digit 14:  99.65%,   0.21%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.02%,   0.12%
    Statistical Distance: 0.5964928688
    Kullback Distance: 3.2839577371
        Minimum Entropy: 0.0050686141 bits
    Digit 15:  99.91%,   0.00%,   0.07%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.02%,   0.00%
    Statistical Distance: 0.5490647650
    Kullback Distance: 3.3104100385
        Minimum Entropy: 0.0013498902 bits
    Digit 16:  99.98%,   0.00%,   0.00%,   0.00%,   0.00%,   0.02%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4997661913
    Kullback Distance: 3.3187705289
        Minimum Entropy: 0.0003373542 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   4.96%,   1.08%,   0.07%,  37.43%,   0.02%,   8.00%,  20.69%,   0.02%,   0.14%,  27.59%
    Statistical Distance: 0.5571428571
    Kullback Distance: 1.2055257441
        Minimum Entropy: 1.4176258994 bits
    Digit  1:   4.07%,  37.15%,   0.47%,  27.61%,   0.00%,   0.09%,   8.88%,   0.42%,  21.30%,   0.00%
    Statistical Distance: 0.4606499883
    Kullback Distance: 1.2262852519
        Minimum Entropy: 1.4284800825 bits
    Digit  2:   4.16%,   0.00%,  46.04%,   0.00%,   0.00%,   0.89%,   0.00%,  48.91%,   0.00%,   0.00%
    Statistical Distance: 0.4494973112
    Kullback Distance: 2.0506391945
        Minimum Entropy: 1.0317163556 bits
    Digit  3:   5.05%,   0.00%,   0.00%,   0.00%,   0.00%,  94.95%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4494973112
    Kullback Distance: 3.0333995776
        Minimum Entropy: 0.0747641800 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   3.83%,  21.74%,   0.09%,  14.10%,   0.05%,  26.93%,   0.63%,  13.70%,   1.08%,  17.84%
    Statistical Distance: 0.4431844751
    Kullback Distance: 0.7870754156
        Minimum Entropy: 1.8924584904 bits
    Digit  1:   3.88%,   7.55%,  14.43%,  17.89%,   0.00%,  13.79%,  26.89%,   7.62%,   7.95%,   0.00%
    Statistical Distance: 0.2299509002
    Kullback Distance: 0.5342515525
        Minimum Entropy: 1.8949653460 bits
    Digit  2:  17.68%,   0.00%,  34.44%,   0.00%,   0.00%,  22.05%,   0.00%,  25.84%,   0.00%,   0.00%
    Statistical Distance: 0.3000000000
    Kullback Distance: 1.3649826570
        Minimum Entropy: 1.5378417768 bits
    Digit  3:  39.72%,   0.00%,   0.00%,   0.00%,   0.00%,  60.28%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.3526142745
        Minimum Entropy: 0.7303469435 bits
    Digit  4: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  5: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  6: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  7: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  8: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  9: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 10: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 11: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 12: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 13: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 14: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 2m 51s

    Parsing file: Data/2018_07_17_19_37_40_geomagnetic.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:   0.00%,   0.00%,   0.00%,   0.00%,   0.08%,  98.93%,   0.99%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5393306379
    Kullback Distance: 3.2326801843
        Minimum Entropy: 0.0154753394 bits
    Digit  3:   1.46%,   5.76%,  15.84%,  24.98%,  25.16%,  15.30%,   6.50%,   2.17%,   1.34%,   1.50%
    Statistical Distance: 0.4126684636
    Kullback Distance: 0.6090401945
        Minimum Entropy: 1.9909548604 bits
    Digit  4:   9.64%,   9.74%,  10.34%,   9.87%,  10.11%,   9.80%,  10.22%,  10.04%,  10.12%,  10.12%
    Statistical Distance: 0.0095013477
    Kullback Distance: 0.0003353935
        Minimum Entropy: 3.2731805313 bits
    Digit  5:  10.25%,   9.87%,  10.30%,   9.33%,  10.42%,   9.50%,   9.98%,  10.23%,  10.23%,   9.87%
    Statistical Distance: 0.0143755615
    Kullback Distance: 0.0008480930
        Minimum Entropy: 3.2622568822 bits
    Digit  6:   9.56%,  10.05%,  10.53%,  10.06%,  10.18%,   9.85%,   9.77%,  10.00%,   9.86%,  10.14%
    Statistical Distance: 0.0096585804
    Kullback Distance: 0.0004557554
        Minimum Entropy: 3.2467937648 bits
    Digit  7:  10.25%,   9.36%,  10.39%,  10.01%,  10.09%,  10.28%,   9.85%,  10.24%,   9.41%,  10.13%
    Statistical Distance: 0.0138364780
    Kullback Distance: 0.0008454902
        Minimum Entropy: 3.2669283219 bits
    Digit  8:  10.30%,   9.61%,  10.44%,  10.06%,  10.20%,   9.93%,   9.74%,   9.98%,   9.59%,  10.14%
    Statistical Distance: 0.0114555256
    Kullback Distance: 0.0005350717
        Minimum Entropy: 3.2591509713 bits
    Digit  9:   9.94%,  10.71%,  10.58%,   9.71%,   9.88%,   9.85%,  10.07%,   9.49%,   9.76%,  10.00%
    Statistical Distance: 0.0136792453
    Kullback Distance: 0.0009222854
        Minimum Entropy: 3.2223924213 bits
    Digit 10:   9.89%,   9.83%,   9.83%,  10.22%,   9.55%,  10.11%,   9.94%,  10.50%,  10.03%,  10.11%
    Statistical Distance: 0.0096585804
    Kullback Distance: 0.0004349976
        Minimum Entropy: 3.2514153226 bits
    Digit 11:   9.66%,  10.02%,   9.58%,   9.68%,  10.30%,  10.14%,   9.89%,   9.92%,  10.48%,  10.33%
    Statistical Distance: 0.0126909254
    Kullback Distance: 0.0006211409
        Minimum Entropy: 3.2545046065 bits
    Digit 12:   9.78%,  10.48%,  10.03%,   9.84%,   9.93%,   9.97%,   9.80%,   9.98%,  10.34%,   9.84%
    Statistical Distance: 0.0085130279
    Kullback Distance: 0.0003519966
        Minimum Entropy: 3.2545046065 bits
    Digit 13:  10.01%,   9.86%,  10.39%,  10.07%,  10.56%,   9.65%,   9.50%,  10.07%,   9.88%,  10.01%
    Statistical Distance: 0.0110736748
    Kullback Distance: 0.0006324499
        Minimum Entropy: 3.2437209308 bits
    Digit 14:   9.89%,  10.22%,  10.29%,  10.13%,   9.68%,   9.88%,   9.91%,  10.42%,  10.09%,   9.49%
    Statistical Distance: 0.0114555256
    Kullback Distance: 0.0005274283
        Minimum Entropy: 3.2622568822 bits
    Digit 15:  10.86%,   9.51%,   9.64%,   9.64%,  10.09%,  10.31%,  10.13%,   9.78%,  10.29%,   9.76%
    Statistical Distance: 0.0167340521
    Kullback Distance: 0.0011084020
        Minimum Entropy: 3.2028657979 bits
    Digit 16:   9.77%,  10.37%,   9.80%,   9.64%,   9.83%,  10.66%,  10.40%,  10.21%,   9.34%,   9.98%
    Statistical Distance: 0.0163297394
    Kullback Distance: 0.0010481976
        Minimum Entropy: 3.2299736003 bits
    Digit 17:   9.88%,   9.79%,   9.77%,  10.39%,   9.85%,  10.91%,   9.66%,  10.27%,   9.69%,   9.79%
    Statistical Distance: 0.0155884996
    Kullback Distance: 0.0010123560
        Minimum Entropy: 3.1969103919 bits
    Digit 18:  86.92%,   0.00%,   0.00%,   0.00%,   1.53%,  10.29%,   1.27%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4720350404
    Kullback Distance: 2.6364563099
        Minimum Entropy: 0.2023064331 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  2:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,  99.98%,   0.02%,   0.00%,   0.00%
    Statistical Distance: 0.4997753819
    Kullback Distance: 3.3188816504
        Minimum Entropy: 0.0003240919 bits
    Digit  3:   0.02%,   0.01%,   0.77%,   5.90%,  16.60%,  24.17%,  25.52%,  20.22%,   6.21%,   0.58%
    Statistical Distance: 0.4650044924
    Kullback Distance: 0.8359756160
        Minimum Entropy: 1.9704907578 bits
    Digit  4:   9.80%,  10.11%,   9.88%,  10.09%,  10.00%,  10.21%,   9.82%,   9.82%,   9.96%,  10.32%
    Statistical Distance: 0.0072327044
    Kullback Distance: 0.0002064167
        Minimum Entropy: 3.2763168261 bits
    Digit  5:  10.24%,   9.31%,   9.47%,  10.31%,  10.15%,   9.89%,   9.87%,  10.40%,   9.97%,  10.38%
    Statistical Distance: 0.0148247978
    Kullback Distance: 0.0009225156
        Minimum Entropy: 3.2653694941 bits
    Digit  6:  10.13%,  10.51%,  10.04%,  10.13%,   9.42%,   9.74%,  10.47%,   9.79%,  10.27%,   9.50%
    Statistical Distance: 0.0154537287
    Kullback Distance: 0.0009257451
        Minimum Entropy: 3.2498731578 bits
    Digit  7:   9.74%,   9.69%,  10.25%,  10.29%,   9.94%,   9.15%,  10.29%,  10.22%,  10.73%,   9.70%
    Statistical Distance: 0.0177448338
    Kullback Distance: 0.0012882621
        Minimum Entropy: 3.2208809544 bits
    Digit  8:  10.10%,   9.43%,  10.42%,   9.89%,   9.84%,  10.04%,  10.13%,   9.85%,  10.14%,  10.15%
    Statistical Distance: 0.0098382749
    Kullback Distance: 0.0004571202
        Minimum Entropy: 3.2622568822 bits
    Digit  9:   9.28%,  10.41%,  10.16%,  10.01%,   9.84%,   9.97%,  10.10%,  10.39%,   9.84%,  10.01%
    Statistical Distance: 0.0107367475
    Kullback Distance: 0.0006791042
        Minimum Entropy: 3.2638123487 bits
    Digit 10:   9.92%,   9.66%,   9.66%,  10.46%,   9.91%,  10.21%,   9.91%,   9.73%,  10.13%,  10.43%
    Statistical Distance: 0.0122866128
    Kullback Distance: 0.0005675135
        Minimum Entropy: 3.2576005198 bits
    Digit 11:  10.49%,   9.80%,   9.60%,  10.59%,  10.75%,   9.67%,   9.77%,  10.18%,   9.91%,   9.24%
    Statistical Distance: 0.0200359389
    Kullback Distance: 0.0015245931
        Minimum Entropy: 3.2178627629 bits
    Digit 12:   9.83%,  10.25%,  10.28%,  10.32%,   9.25%,   9.79%,   9.92%,  10.28%,  10.01%,  10.07%
    Statistical Distance: 0.0120844564
    Kullback Distance: 0.0007020633
        Minimum Entropy: 3.2763168261 bits
    Digit 13:  10.51%,   9.76%,   9.79%,   9.86%,   9.94%,   9.74%,  10.21%,  10.01%,  10.10%,  10.09%
    Statistical Distance: 0.0090970350
    Kullback Distance: 0.0003692854
        Minimum Entropy: 3.2498731578 bits
    Digit 14:   9.43%,  10.10%,   9.96%,   9.87%,  10.12%,  10.46%,   9.96%,  10.70%,   9.79%,   9.60%
    Statistical Distance: 0.0137466307
    Kullback Distance: 0.0009092866
        Minimum Entropy: 3.2239054734 bits
    Digit 15:   9.91%,  10.11%,   9.91%,  10.34%,  10.03%,   9.56%,  10.22%,  10.10%,   9.88%,   9.95%
    Statistical Distance: 0.0079739443
    Kullback Distance: 0.0003025111
        Minimum Entropy: 3.2731805313 bits
    Digit 16:   9.94%,  10.15%,  10.16%,  10.07%,   9.74%,   9.61%,  10.34%,  10.21%,  10.57%,   9.20%
    Statistical Distance: 0.0151168014
    Kullback Distance: 0.0010229160
        Minimum Entropy: 3.2421869646 bits
    Digit 17:  10.38%,   9.62%,   9.73%,  10.20%,   9.87%,   9.93%,  10.18%,  10.20%,  10.37%,   9.54%
    Statistical Distance: 0.0131401617
    Kullback Distance: 0.0006064994
        Minimum Entropy: 3.2684888359 bits
    Digit 18:  86.90%,   0.00%,   0.00%,   0.00%,   1.67%,   9.84%,   1.58%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4690476190
    Kullback Distance: 2.6233610653
        Minimum Entropy: 0.2024928639 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%, 100.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   0.00%,   0.00%,  85.94%,  14.06%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4000000000
    Kullback Distance: 2.7360914642
        Minimum Entropy: 0.2186167190 bits
    Digit  2:  14.06%,   0.00%,   0.00%,   0.00%,   9.46%,  14.93%,  15.12%,  15.46%,  15.55%,  15.42%
    Statistical Distance: 0.1554357592
    Kullback Distance: 0.5306385347
        Minimum Entropy: 2.6845676164 bits
    Digit  3:   9.34%,   9.34%,   9.16%,   9.48%,  10.50%,  10.65%,  10.83%,  10.69%,  10.76%,   9.24%
    Statistical Distance: 0.0342542677
    Kullback Distance: 0.0034731160
        Minimum Entropy: 3.2073485411 bits
    Digit  4:   9.82%,  10.15%,   9.85%,  10.33%,   9.78%,   9.87%,   9.92%,  10.05%,  10.19%,  10.04%
    Statistical Distance: 0.0076370171
    Kullback Distance: 0.0002160200
        Minimum Entropy: 3.2747478264 bits
    Digit  5:   9.89%,   9.89%,   9.87%,   9.89%,  10.29%,   9.82%,  10.25%,  10.25%,  10.01%,   9.83%
    Statistical Distance: 0.0080188679
    Kullback Distance: 0.0002336714
        Minimum Entropy: 3.2810340892 bits
    Digit  6:   9.93%,  10.62%,  10.18%,   9.67%,  10.09%,  10.21%,  10.01%,   9.34%,   9.71%,  10.24%
    Statistical Distance: 0.0134321653
    Kullback Distance: 0.0008361073
        Minimum Entropy: 3.2345415040 bits
    Digit  7:  10.32%,  10.21%,  10.32%,  10.34%,   9.79%,  10.65%,   9.41%,   9.41%,  10.12%,   9.42%
    Statistical Distance: 0.0196091644
    Kullback Distance: 0.0013544346
        Minimum Entropy: 3.2314946284 bits
    Digit  8:   9.59%,   9.74%,   9.92%,  10.22%,   9.96%,   9.75%,   9.58%,  10.22%,  10.95%,  10.07%
    Statistical Distance: 0.0146451033
    Kullback Distance: 0.0010585268
        Minimum Entropy: 3.1909794687 bits
    Digit  9:   9.93%,  10.10%,  10.31%,   9.85%,   9.66%,  10.11%,   9.36%,  10.58%,  10.32%,   9.79%
    Statistical Distance: 0.0141509434
    Kullback Distance: 0.0008376808
        Minimum Entropy: 3.2406546277 bits
    Digit 10:   9.38%,  10.16%,   9.79%,   9.84%,  10.46%,   9.82%,  10.25%,  10.33%,   9.94%,  10.03%
    Statistical Distance: 0.0123539982
    Kullback Distance: 0.0006550844
        Minimum Entropy: 3.2576005198 bits
    Digit 11:  10.02%,  10.16%,   9.52%,   9.79%,  10.66%,   9.85%,  10.20%,  10.38%,   9.77%,   9.65%
    Statistical Distance: 0.0141509434
    Kullback Distance: 0.0007973869
        Minimum Entropy: 3.2299736003 bits
    Digit 12:  10.66%,   9.21%,  10.14%,  10.29%,  10.32%,   9.92%,   9.88%,  10.12%,   9.54%,   9.93%
    Statistical Distance: 0.0152740341
    Kullback Distance: 0.0011034201
        Minimum Entropy: 3.2299736003 bits
    Digit 13:   9.65%,  10.52%,  10.18%,  10.29%,   9.76%,  10.01%,   9.64%,  10.34%,   9.77%,   9.85%
    Statistical Distance: 0.0133647799
    Kullback Distance: 0.0006436286
        Minimum Entropy: 3.2483326397 bits
    Digit 14:   9.88%,   9.80%,  10.32%,   9.79%,  10.02%,  10.02%,   9.78%,  10.39%,  10.03%,   9.96%
    Statistical Distance: 0.0077493261
    Kullback Distance: 0.0002864336
        Minimum Entropy: 3.2669283219 bits
    Digit 15:  11.15%,   9.09%,   9.77%,  10.12%,   9.93%,   9.47%,  10.23%,  10.48%,  10.20%,   9.57%
    Statistical Distance: 0.0217879605
    Kullback Distance: 0.0021703356
        Minimum Entropy: 3.1645879698 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Wrote 109599 bytes (876792 bits) to file: "Data/2018_07_17_19_37_40_geomagnetic.csv.bin"
    Generated 876792 bits of entropy from data collected over 5m 56s

    Parsing file: Data/2018_07_17_19_44_26_linearacceleration.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:  83.93%,  15.37%,   0.68%,   0.02%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4929751048
    Kullback Distance: 2.6427491654
        Minimum Entropy: 0.2527585043 bits
    Digit  2:  15.90%,  14.47%,  13.21%,  12.26%,  11.08%,   9.50%,   7.58%,   5.73%,   5.23%,   5.04%
    Statistical Distance: 0.1692137047
    Kullback Distance: 0.1069336980
        Minimum Entropy: 2.6530422369 bits
    Digit  3:  10.23%,  10.11%,  10.25%,  10.35%,   9.83%,   9.63%,   9.91%,  10.12%,   9.81%,   9.76%
    Statistical Distance: 0.0105989648
    Kullback Distance: 0.0003865095
        Minimum Entropy: 3.2719520695 bits
    Digit  4:  10.18%,   9.59%,   9.91%,   9.32%,  10.43%,  10.09%,  10.20%,  10.16%,   9.85%,  10.28%
    Statistical Distance: 0.0133842741
    Kullback Distance: 0.0007527011
        Minimum Entropy: 3.2616837341 bits
    Digit  5:  10.28%,  10.06%,   9.64%,  10.67%,  10.06%,  10.09%,  10.02%,   9.37%,   9.88%,   9.93%
    Statistical Distance: 0.0117821050
    Kullback Distance: 0.0007907817
        Minimum Entropy: 3.2279743725 bits
    Digit  6:  10.19%,   9.71%,   9.60%,   9.90%,  11.04%,   9.60%,   9.79%,   9.49%,   9.91%,  10.77%
    Statistical Distance: 0.0200640868
    Kullback Distance: 0.0017354801
        Minimum Entropy: 3.1788426651 bits
    Digit  7:  10.04%,  10.08%,   9.53%,   9.95%,  10.20%,   9.60%,   9.65%,  10.41%,  10.76%,   9.77%
    Statistical Distance: 0.0150357407
    Kullback Distance: 0.0009734784
        Minimum Entropy: 3.2163597436 bits
    Digit  8:  10.20%,  10.65%,   9.45%,   9.80%,   9.76%,  10.35%,   9.87%,  10.12%,  10.00%,   9.80%
    Statistical Distance: 0.0132363816
    Kullback Distance: 0.0007582987
        Minimum Entropy: 3.2313100850 bits
    Digit  9:  10.38%,   9.53%,  10.22%,   9.58%,  10.32%,   9.90%,   9.88%,  10.36%,   9.48%,  10.36%
    Statistical Distance: 0.0163914222
    Kullback Distance: 0.0009094586
        Minimum Entropy: 3.2685211641 bits
    Digit 10:  10.55%,   9.81%,  10.09%,  10.03%,   9.51%,  10.50%,   9.93%,   9.70%,  10.11%,   9.76%
    Statistical Distance: 0.0128173527
    Kullback Distance: 0.0007165685
        Minimum Entropy: 3.2447306008 bits
    Digit 11:   9.70%,  10.38%,  10.16%,  10.53%,   9.43%,  10.25%,  10.04%,   9.56%,   9.85%,  10.11%
    Statistical Distance: 0.0146167119
    Kullback Distance: 0.0008323693
        Minimum Entropy: 3.2481053276 bits
    Digit 12:  10.36%,   9.91%,   9.69%,  10.01%,   9.87%,  10.11%,   9.92%,  10.13%,  10.08%,   9.92%
    Statistical Distance: 0.0069016515
    Kullback Distance: 0.0002183381
        Minimum Entropy: 3.2702355970 bits
    Digit 13:   9.93%,  10.01%,  10.13%,   9.77%,  10.20%,  10.00%,  10.20%,   9.91%,  10.34%,   9.50%
    Statistical Distance: 0.0088735519
    Kullback Distance: 0.0003832941
        Minimum Entropy: 3.2736705868 bits
    Digit 14:  10.24%,   9.77%,  10.45%,  10.09%,   9.75%,   9.79%,   9.43%,  10.29%,  10.11%,  10.08%
    Statistical Distance: 0.0126448114
    Kullback Distance: 0.0006232455
        Minimum Entropy: 3.2582771326 bits
    Digit 15:   9.90%,  10.01%,   9.90%,   9.90%,   9.56%,  10.17%,  10.30%,  10.33%,  10.40%,   9.54%
    Statistical Distance: 0.0120778901
    Kullback Distance: 0.0005957257
        Minimum Entropy: 3.2650983985 bits
    Digit 16:   9.90%,  10.24%,  10.18%,   9.85%,  10.01%,   9.54%,  10.28%,  10.30%,   9.43%,  10.28%
    Statistical Distance: 0.0128912990
    Kullback Distance: 0.0006624326
        Minimum Entropy: 3.2788384551 bits
    Digit 17:  11.94%,   9.55%,  10.04%,   9.72%,  10.06%,   9.56%,  10.12%,  10.41%,  10.08%,   8.50%
    Statistical Distance: 0.0265713581
    Kullback Distance: 0.0047452478
        Minimum Entropy: 3.0658447318 bits
    Digit 18:  65.37%,   1.40%,   2.65%,   4.01%,   5.46%,   7.04%,   4.79%,   4.55%,   3.08%,   1.65%
    Statistical Distance: 0.5536849889
    Kullback Distance: 1.3460170888
        Minimum Entropy: 0.6133325271 bits
    Digit 19:  96.61%,   0.16%,   0.25%,   0.46%,   0.55%,   0.53%,   0.54%,   0.53%,   0.30%,   0.07%
    Statistical Distance: 0.8661079615
    Kullback Distance: 3.0071271404
        Minimum Entropy: 0.0497436772 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:  84.04%,  15.33%,   0.63%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4437145674
    Kullback Distance: 2.6503578762
        Minimum Entropy: 0.2508531177 bits
    Digit  2:  16.34%,  13.32%,  12.85%,  11.98%,  11.38%,   8.65%,   8.68%,   6.38%,   5.69%,   4.72%
    Statistical Distance: 0.1587379837
    Kullback Distance: 0.0952940262
        Minimum Entropy: 2.6133325271 bits
    Digit  3:   9.91%,  10.49%,  10.40%,  10.44%,   9.91%,  10.00%,   9.87%,   9.95%,   9.60%,   9.44%
    Statistical Distance: 0.0132856791
    Kullback Distance: 0.0007936992
        Minimum Entropy: 3.2531822655 bits
    Digit  4:   9.75%,   9.88%,  10.73%,   9.79%,  10.09%,   9.64%,  10.01%,   9.85%,   9.97%,  10.29%
    Statistical Distance: 0.0112644811
    Kullback Distance: 0.0006493384
        Minimum Entropy: 3.2196686786 bits
    Digit  5:   9.61%,  10.32%,  10.17%,   9.45%,  10.16%,  10.08%,  10.00%,  10.27%,  10.08%,   9.87%
    Statistical Distance: 0.0106729110
    Kullback Distance: 0.0005101229
        Minimum Entropy: 3.2771137747 bits
    Digit  6:   9.93%,   9.72%,   9.77%,  10.32%,  10.08%,  10.27%,   9.95%,   9.79%,   9.92%,  10.25%
    Statistical Distance: 0.0091693369
    Kullback Distance: 0.0003087047
        Minimum Entropy: 3.2771137747 bits
    Digit  7:  10.23%,   9.95%,   9.40%,  10.07%,   9.92%,  10.11%,  10.43%,  10.24%,  10.30%,   9.35%
    Statistical Distance: 0.0137540054
    Kullback Distance: 0.0008619494
        Minimum Entropy: 3.2616837341 bits
    Digit  8:  10.33%,  10.03%,   9.93%,  10.03%,   9.87%,  10.27%,   9.87%,  10.22%,   9.76%,   9.69%
    Statistical Distance: 0.0087503081
    Kullback Distance: 0.0003025488
        Minimum Entropy: 3.2753911535 bits
    Digit  9:  10.08%,  10.53%,  10.08%,   9.48%,   9.82%,  10.12%,  10.38%,   9.98%,   9.30%,  10.23%
    Statistical Distance: 0.0141237368
    Kullback Distance: 0.0009344264
        Minimum Entropy: 3.2481053276 bits
    Digit 10:  10.14%,  10.60%,  10.36%,   9.77%,   9.55%,   9.72%,   9.83%,   9.53%,  10.16%,  10.33%
    Statistical Distance: 0.0158984471
    Kullback Distance: 0.0008815049
        Minimum Entropy: 3.2380047376 bits
    Digit 11:  10.28%,  10.43%,   9.56%,   9.83%,  10.22%,   9.82%,  10.09%,  10.34%,   9.43%,  10.00%
    Statistical Distance: 0.0135568154
    Kullback Distance: 0.0007298491
        Minimum Entropy: 3.2616837341 bits
    Digit 12:   9.67%,  10.02%,   9.91%,   9.88%,  10.16%,  10.39%,  10.48%,   9.56%,   9.93%,  10.00%
    Statistical Distance: 0.0104017747
    Kullback Distance: 0.0005218261
        Minimum Entropy: 3.2548785562 bits
    Digit 13:   9.65%,   9.66%,   9.50%,  10.62%,   9.83%,  10.25%,  10.40%,   9.58%,  10.07%,  10.43%
    Statistical Distance: 0.0177471038
    Kullback Distance: 0.0010744651
        Minimum Entropy: 3.2346535281 bits
    Digit 14:   9.79%,   9.97%,   9.44%,  10.34%,  10.48%,  10.60%,   9.82%,  10.14%,  10.17%,   9.26%
    Statistical Distance: 0.0172541287
    Kullback Distance: 0.0012289332
        Minimum Entropy: 3.2380047376 bits
    Digit 15:  10.13%,   9.48%,  10.43%,  10.00%,  10.53%,   9.58%,  10.02%,   9.87%,  10.19%,   9.79%
    Statistical Distance: 0.0129405965
    Kullback Distance: 0.0007411737
        Minimum Entropy: 3.2481053276 bits
    Digit 16:  10.69%,   9.95%,   9.72%,   9.63%,  10.38%,  10.08%,  10.38%,   9.70%,   9.90%,   9.59%
    Statistical Distance: 0.0152082820
    Kullback Distance: 0.0008963899
        Minimum Entropy: 3.2263094040 bits
    Digit 17:  12.51%,   8.94%,  10.11%,  10.01%,  10.35%,   9.83%,   9.58%,  10.51%,   9.44%,   8.73%
    Statistical Distance: 0.0348779887
    Kullback Distance: 0.0069412788
        Minimum Entropy: 2.9989335751 bits
    Digit 18:  66.12%,   1.64%,   2.87%,   4.52%,   4.83%,   7.67%,   4.40%,   3.52%,   2.70%,   1.73%
    Statistical Distance: 0.5612028593
    Kullback Distance: 1.3756285158
        Minimum Entropy: 0.5968351316 bits
    Digit 19:  95.69%,   0.12%,   0.39%,   0.54%,   0.80%,   0.63%,   0.65%,   0.58%,   0.38%,   0.21%
    Statistical Distance: 0.8568646783
    Kullback Distance: 2.9352917092
        Minimum Entropy: 0.0636131845 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit  1:   0.00%,   0.01%,   0.00%,   0.12%,   5.87%,  43.94%,  43.23%,   6.54%,   0.26%,   0.02%
    Statistical Distance: 0.5717032290
    Kullback Distance: 1.7414383944
        Minimum Entropy: 1.1865112259 bits
    Digit  2:   9.16%,   8.61%,  15.21%,   8.63%,   8.58%,   8.64%,   8.22%,  16.29%,   7.95%,   8.71%
    Statistical Distance: 0.1150110919
    Kullback Distance: 0.0540919820
        Minimum Entropy: 2.6176911257 bits
    Digit  3:   8.70%,   9.92%,   9.59%,  11.70%,   9.71%,  12.63%,   9.27%,   9.88%,   9.13%,   9.47%
    Statistical Distance: 0.0432832142
    Kullback Distance: 0.0092148244
        Minimum Entropy: 2.9847893928 bits
    Digit  4:   9.80%,  10.24%,  10.49%,  10.07%,   9.69%,  10.16%,   9.39%,   9.56%,   9.95%,  10.66%
    Statistical Distance: 0.0161449347
    Kullback Distance: 0.0010551971
        Minimum Entropy: 3.2296412647 bits
    Digit  5:   9.93%,   9.74%,   9.18%,  10.29%,  10.32%,  10.20%,  10.25%,   9.74%,  10.54%,   9.81%
    Statistical Distance: 0.0160216909
    Kullback Distance: 0.0010396585
        Minimum Entropy: 3.2464169774 bits
    Digit  6:   9.35%,   9.86%,  10.04%,  10.55%,  10.40%,  10.24%,  10.06%,   9.59%,  10.09%,   9.81%
    Statistical Distance: 0.0138772492
    Kullback Distance: 0.0008526977
        Minimum Entropy: 3.2447306008 bits
    Digit  7:  10.13%,  10.38%,   9.70%,   9.88%,  10.36%,   9.70%,   9.24%,   9.88%,  10.73%,   9.98%
    Statistical Distance: 0.0160709884
    Kullback Distance: 0.0011637686
        Minimum Entropy: 3.2196686786 bits
    Digit  8:   8.89%,   9.87%,  10.16%,  10.32%,   9.93%,  10.12%,   9.98%,  10.91%,  10.22%,   9.61%
    Statistical Distance: 0.0171308849
    Kullback Distance: 0.0017638546
        Minimum Entropy: 3.1966639422 bits
    Digit  9:  10.20%,   9.96%,  10.24%,   9.66%,   9.76%,   9.38%,   9.85%,  10.67%,   9.67%,  10.60%
    Statistical Distance: 0.0171801824
    Kullback Distance: 0.0011494970
        Minimum Entropy: 3.2279743725 bits
    Digit 10:   9.49%,  10.27%,  10.13%,   9.63%,  10.50%,  10.18%,  10.19%,   9.77%,   9.98%,   9.86%
    Statistical Distance: 0.0126941089
    Kullback Distance: 0.0006358178
        Minimum Entropy: 3.2514879670 bits
    Digit 11:  10.07%,  10.27%,   9.93%,   9.87%,  10.18%,   9.98%,   9.81%,   9.60%,   9.83%,  10.45%
    Statistical Distance: 0.0096623121
    Kullback Distance: 0.0003997589
        Minimum Entropy: 3.2582771326 bits
    Digit 12:   9.92%,   9.75%,   9.90%,  10.49%,   9.63%,  10.18%,  10.50%,  10.22%,   9.43%,  10.00%
    Statistical Distance: 0.0138526004
    Kullback Distance: 0.0008049887
        Minimum Entropy: 3.2514879670 bits
    Digit 13:   9.66%,   9.86%,  10.43%,  10.35%,   9.69%,  10.02%,  10.35%,   9.69%,   9.63%,  10.33%
    Statistical Distance: 0.0147892531
    Kullback Distance: 0.0007264084
        Minimum Entropy: 3.2616837341 bits
    Digit 14:   9.85%,   9.53%,   9.80%,  10.19%,  10.59%,  10.00%,  10.80%,   9.44%,  10.45%,   9.37%
    Statistical Distance: 0.0202612768
    Kullback Distance: 0.0015969409
        Minimum Entropy: 3.2114105276 bits
    Digit 15:   9.79%,   9.13%,   9.90%,  10.19%,   9.61%,   9.96%,  10.77%,   9.23%,  10.25%,  11.17%
    Statistical Distance: 0.0238353463
    Kullback Distance: 0.0025851555
        Minimum Entropy: 3.1628303472 bits
    Digit 16:  11.14%,   9.24%,   9.76%,   9.71%,  10.48%,  10.50%,   9.45%,  10.33%,  10.36%,   9.02%
    Statistical Distance: 0.0280995810
    Kullback Distance: 0.0028761215
        Minimum Entropy: 3.1660186248 bits
    Digit 17:  79.50%,   0.00%,   0.00%,   0.25%,   6.72%,   6.85%,   6.51%,   0.17%,   0.00%,   0.00%
    Statistical Distance: 0.4950456002
    Kullback Distance: 2.2384677819
        Minimum Entropy: 0.3308904857 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Wrote 107278 bytes (858224 bits) to file: "Data/2018_07_17_19_44_26_linearacceleration.csv.bin"
    Generated 858224 bits of entropy from data collected over 5m 24s

    Parsing file: Data/2018_07_17_19_52_38_accelerometeuncalibrated.csv
    Digit criteria: SD < 0.1000000000

    Axis X:        0        1        2        3        4        5        6        7        8        9
    Digit  0:  99.95%,   0.01%,   0.00%,   0.01%,   0.00%,   0.00%,   0.00%,   0.00%,   0.01%,   0.02%
    Statistical Distance: 0.6495169549
    Kullback Distance: 3.3149819887
        Minimum Entropy: 0.0006970552 bits
    Digit  1:   0.00%,   0.02%,   0.26%,   8.52%,  50.09%,  37.70%,   3.34%,   0.03%,   0.02%,   0.02%
    Statistical Distance: 0.6278861946
    Kullback Distance: 1.7921920315
        Minimum Entropy: 0.9973542577 bits
    Digit  2:   0.00%,   0.04%,  28.70%,  21.17%,   0.01%,   0.00%,   0.02%,  42.59%,   7.47%,   0.01%
    Statistical Distance: 0.5245483528
    Kullback Distance: 1.5175570473
        Minimum Entropy: 1.2315740228 bits
    Digit  3:   0.42%,  19.84%,   2.95%,   7.25%,  11.92%,   1.30%,  25.77%,   0.22%,   0.02%,  30.32%
    Statistical Distance: 0.4784658487
    Kullback Distance: 0.9063481862
        Minimum Entropy: 1.7218528944 bits
    Digit  4:  30.26%,  19.85%,   7.25%,   1.29%,   0.22%,   0.06%,   0.41%,   2.97%,  11.92%,  25.78%
    Statistical Distance: 0.4780794126
    Kullback Distance: 0.9029159861
        Minimum Entropy: 1.7246140341 bits
    Digit  5:  99.95%,   0.01%,   0.00%,   0.01%,   0.00%,   0.00%,   0.01%,   0.01%,   0.01%,   0.00%
    Statistical Distance: 0.6995169549
    Kullback Distance: 3.3147887706
        Minimum Entropy: 0.0006970552 bits
    Digit  6:   0.01%,   0.09%,   1.52%,  57.33%,  37.69%,   3.35%,   0.00%,   0.02%,   0.00%,   0.00%
    Statistical Distance: 0.6001497440
    Kullback Distance: 2.0627920681
        Minimum Entropy: 0.8026933199 bits
    Digit  7:   0.02%,  10.18%,   0.00%,  25.99%,   0.01%,  20.24%,  11.93%,   1.28%,   0.01%,  30.34%
    Statistical Distance: 0.4367645638
    Kullback Distance: 1.0409462471
        Minimum Entropy: 1.7209336876 bits
    Digit  8:   2.96%,  25.77%,  20.23%,  30.27%,   7.48%,   0.00%,   1.33%,   0.03%,   0.01%,  11.93%
    Statistical Distance: 0.4319437735
    Kullback Distance: 0.9459520699
        Minimum Entropy: 1.7241534770 bits
    Digit  9:  19.85%,   7.24%,   1.28%,  11.96%,  25.79%,   0.41%,  30.48%,   0.01%,   0.04%,   2.94%
    Statistical Distance: 0.4808810743
    Kullback Distance: 0.9234710017
        Minimum Entropy: 1.7140582415 bits
    Digit 10:  50.17%,   0.02%,   0.01%,   7.64%,  14.87%,   0.00%,   1.29%,  25.99%,   0.00%,   0.01%
    Statistical Distance: 0.5102502174
    Kullback Distance: 1.5389699348
        Minimum Entropy: 0.9951300213 bits
    Digit 11:   0.01%,   2.94%,   0.05%,   0.22%,   0.00%,  56.04%,  11.94%,   0.42%,   1.29%,  27.09%
    Statistical Distance: 0.6007293981
    Kullback Distance: 1.6874990226
        Minimum Entropy: 0.8353966274 bits
    Digit 12:  12.16%,   7.25%,  30.27%,   0.00%,   0.41%,  19.85%,  25.80%,   4.23%,   0.02%,   0.01%
    Statistical Distance: 0.4308810743
    Kullback Distance: 0.9596424235
        Minimum Entropy: 1.7241534770 bits
    Digit 13:   0.01%,   0.07%,   8.74%,  75.86%,  15.27%,   0.01%,   0.03%,   0.01%,   0.00%,   0.00%
    Statistical Distance: 0.6113129166
    Kullback Distance: 2.2837023830
        Minimum Entropy: 0.3986380739 bits
    Digit 14:   1.28%,   0.42%,  30.27%,   3.17%,  19.83%,   0.02%,  11.96%,   7.25%,   0.03%,  25.78%
    Statistical Distance: 0.4783692397
    Kullback Distance: 0.9150292491
        Minimum Entropy: 1.7241534770 bits
    Digit 15:   0.02%,   0.05%,  30.88%,  25.79%,  11.92%,   0.00%,   1.31%,   7.24%,  22.79%,   0.00%
    Statistical Distance: 0.4138247512
    Kullback Distance: 1.0781967779
        Minimum Entropy: 1.6954308384 bits
    Digit 16:   0.04%,   0.01%,   0.26%,  28.36%,  56.02%,  15.25%,   0.03%,   0.02%,   0.00%,   0.00%
    Statistical Distance: 0.5964254661
    Kullback Distance: 1.8903534157
        Minimum Entropy: 0.8358941084 bits
    Digit 17:  79.92%,   0.00%,   0.01%,   0.00%,   0.22%,   0.00%,  19.84%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4976813834
    Kullback Distance: 2.5796677542
        Minimum Entropy: 0.3232876646 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Y:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.01%,  99.97%,   0.01%,   0.01%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5997101729
    Kullback Distance: 3.3176444599
        Minimum Entropy: 0.0004181927 bits
    Digit  1:   0.00%,   0.00%,   0.01%,   0.03%,   1.57%,  22.49%,  54.19%,  20.64%,   1.07%,   0.00%
    Statistical Distance: 0.5231426915
    Kullback Distance: 1.7197998149
        Minimum Entropy: 0.8839546637 bits
    Digit  2:   0.00%,   0.00%,  43.73%,   6.04%,   0.00%,   0.00%,   0.17%,  31.99%,  18.05%,   0.02%
    Statistical Distance: 0.4376871800
    Kullback Distance: 1.5654593537
        Minimum Entropy: 1.1931429401 bits
    Digit  3:  17.53%,   0.01%,  11.09%,   0.00%,  16.78%,   0.00%,  26.76%,   0.17%,  27.61%,   0.04%
    Statistical Distance: 0.3977779925
    Kullback Distance: 1.0543207087
        Minimum Entropy: 1.8566923303 bits
    Digit  4:   0.17%,   0.21%,   0.91%,   5.24%,  15.40%,  26.75%,  27.44%,  16.65%,   5.85%,   1.38%
    Statistical Distance: 0.4623321418
    Kullback Distance: 0.8106138974
        Minimum Entropy: 1.8658073170 bits
    Digit  5:   0.01%,  99.98%,   0.01%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.5498067820
    Kullback Distance: 3.3190723248
        Minimum Entropy: 0.0002787817 bits
    Digit  6:   0.00%,   0.20%,  23.87%,  54.20%,  21.53%,   0.17%,   0.00%,   0.00%,   0.02%,   0.00%
    Statistical Distance: 0.4960390300
    Kullback Distance: 1.8361232222
        Minimum Entropy: 0.8836974759 bits
    Digit  7:  15.40%,   1.38%,   0.00%,  27.61%,   5.27%,   5.87%,   0.00%,  26.76%,   0.17%,  17.53%
    Statistical Distance: 0.3730557434
    Kullback Distance: 0.8791500867
        Minimum Entropy: 1.8566923303 bits
    Digit  8:  26.92%,   0.00%,  27.48%,   0.01%,  16.64%,   5.24%,   6.03%,   0.01%,  17.68%,   0.00%
    Statistical Distance: 0.3871606608
    Kullback Distance: 0.9580503483
        Minimum Entropy: 1.8637767817 bits
    Digit  9:  15.41%,  26.75%,  27.44%,   0.01%,  16.64%,   6.03%,   2.28%,   0.00%,   0.17%,   5.27%
    Statistical Distance: 0.4123321418
    Kullback Distance: 0.8450975299
        Minimum Entropy: 1.8658073170 bits
    Digit 10:   5.25%,   0.17%,  16.64%,  15.40%,   0.04%,   6.03%,  26.75%,   0.00%,   2.29%,  27.44%
    Statistical Distance: 0.4122355328
    Kullback Distance: 0.8425521104
        Minimum Entropy: 1.8658073170 bits
    Digit 11:   1.07%,   0.00%,   0.00%,   0.00%,   0.01%,   0.20%,  23.88%,  54.19%,  20.64%,   0.01%
    Statistical Distance: 0.5370543909
    Kullback Distance: 1.7888084030
        Minimum Entropy: 0.8839546637 bits
    Digit 12:   0.00%,  17.69%,   0.03%,  27.44%,   0.00%,  11.27%,   0.18%,  26.75%,   0.00%,  16.64%
    Statistical Distance: 0.3478746015
    Kullback Distance: 1.0534678617
        Minimum Entropy: 1.8658073170 bits
    Digit 13:   1.38%,  23.39%,  27.61%,  42.16%,   5.24%,   0.00%,   0.00%,   0.00%,   0.05%,   0.17%
    Statistical Distance: 0.4816008115
    Kullback Distance: 1.4642616104
        Minimum Entropy: 1.2460471450 bits
    Digit 14:   5.85%,   0.17%,  26.75%,   1.38%,   0.91%,  27.46%,   5.41%,   0.00%,  16.64%,  15.43%
    Statistical Distance: 0.4127185779
    Kullback Distance: 0.8242986090
        Minimum Entropy: 1.8647916921 bits
    Digit 15:  16.64%,  27.44%,  26.75%,  15.40%,   5.24%,   0.00%,   0.03%,   1.07%,   1.58%,   5.85%
    Statistical Distance: 0.4122355328
    Kullback Distance: 0.8241605591
        Minimum Entropy: 1.8658073170 bits
    Digit 16:   0.05%,   0.00%,   1.56%,  49.94%,  42.15%,   6.31%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4708772099
    Kullback Distance: 1.9460547010
        Minimum Entropy: 1.0018130444 bits
    Digit 17:  99.99%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.01%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4999033910
    Kullback Distance: 3.3205002031
        Minimum Entropy: 0.0001393841 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    Axis Z:        0        1        2        3        4        5        6        7        8        9
    Digit  0:   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.01%,  99.98%,   0.01%
    Statistical Distance: 0.5498067820
    Kullback Distance: 3.3190723248
        Minimum Entropy: 0.0002787817 bits
    Digit  1:   0.00%,   0.01%,   0.04%,   1.33%,  14.66%,  44.74%,  32.84%,   6.07%,   0.30%,   0.02%
    Statistical Distance: 0.5723263453
    Kullback Distance: 1.5077016434
        Minimum Entropy: 1.1603744897 bits
    Digit  2:   0.00%,   0.29%,  25.61%,  24.43%,   0.01%,   0.00%,   0.03%,  37.82%,  11.77%,   0.04%
    Statistical Distance: 0.4963288571
    Kullback Distance: 1.3945780223
        Minimum Entropy: 1.4026859392 bits
    Digit  3:   1.36%,  20.23%,   4.71%,  10.61%,  11.94%,   4.08%,  20.94%,   1.18%,   0.27%,  24.68%
    Statistical Distance: 0.3839725630
    Kullback Distance: 0.5965980204
        Minimum Entropy: 2.0183749555 bits
    Digit  4:  24.54%,  20.20%,  10.59%,   4.09%,   1.21%,   0.43%,   1.39%,   4.71%,  11.94%,  20.91%
    Statistical Distance: 0.3817505555
    Kullback Distance: 0.5847442827
        Minimum Entropy: 2.0268697497 bits
    Digit  5:  87.76%,  12.23%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.01%
    Statistical Distance: 0.4499033910
    Kullback Distance: 2.7845622114
        Minimum Entropy: 0.1883705407 bits
    Digit  6:   0.01%,   0.27%,  11.92%,  21.40%,   0.00%,   4.73%,  28.64%,   0.00%,   1.38%,  31.64%
    Statistical Distance: 0.4360448266
    Kullback Distance: 1.1201690613
        Minimum Entropy: 1.6602033401 bits
    Digit  7:   0.03%,  44.74%,  20.91%,  11.94%,   1.18%,   4.09%,  10.59%,   0.01%,   4.71%,   1.81%
    Statistical Distance: 0.4817505555
    Kullback Distance: 1.0405181329
        Minimum Entropy: 1.1603744897 bits
    Digit  8:  11.98%,  11.93%,   0.03%,  20.20%,   8.80%,  20.91%,   0.43%,   0.00%,   1.19%,  24.54%
    Statistical Distance: 0.3455656458
    Kullback Distance: 0.7322064776
        Minimum Entropy: 2.0268697497 bits
    Digit  9:   0.00%,   6.08%,   0.28%,   0.20%,  20.21%,  45.44%,  11.92%,   0.02%,   1.19%,  14.66%
    Statistical Distance: 0.4723263453
    Kullback Distance: 1.2009921311
        Minimum Entropy: 1.1378101865 bits
    Digit 10:  33.34%,  21.39%,   0.01%,   0.00%,   0.00%,   0.01%,   0.01%,   0.00%,  12.22%,  33.02%
    Statistical Distance: 0.4497101729
    Kullback Distance: 1.4153470294
        Minimum Entropy: 1.5846837729 bits
    Digit 11:   0.03%,   0.00%,   0.00%,   0.01%,   0.00%,  21.39%,  29.25%,   5.48%,  31.64%,  12.20%
    Statistical Distance: 0.3948362477
    Kullback Distance: 1.1974421934
        Minimum Entropy: 1.6602033401 bits
    Digit 12:   0.29%,  20.91%,   4.07%,   0.06%,  16.63%,  30.79%,   0.03%,   1.37%,  24.54%,   1.32%
    Statistical Distance: 0.5286059318
    Kullback Distance: 1.0094291377
        Minimum Entropy: 1.6994992276 bits
    Digit 13:   0.02%,   1.21%,  24.43%,  35.13%,  25.61%,  13.28%,   0.29%,   0.03%,   0.00%,   0.00%
    Statistical Distance: 0.4845425563
    Kullback Distance: 1.2976701900
        Minimum Entropy: 1.5093460472 bits
    Digit 14:   0.01%,   0.03%,   0.27%,  11.94%,   1.36%,  36.20%,  24.68%,  24.32%,   1.18%,   0.01%
    Statistical Distance: 0.5714037291
    Kullback Distance: 1.2418895957
        Minimum Entropy: 1.4659622690 bits
    Digit 15: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 16: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 17: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 18: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits
    Digit 19: 100.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%,   0.00%
    Statistical Distance: 0.4500000000
    Kullback Distance: 3.3219280949
        Minimum Entropy: 0.0000000000 bits

    No file was written - No satisfactory data produced
    Generated 0 bits of entropy from data collected over 6m 55s
